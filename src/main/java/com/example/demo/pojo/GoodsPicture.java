package com.example.demo.pojo;

public class GoodsPicture {
    private Integer goodsId;
    private String picture;

    @Override
    public String toString() {
        return "GoodsPicture{" +
                "goodsId=" + goodsId +
                ", picture='" + picture + '\'' +
                '}';
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String pictrue) {
        this.picture = pictrue;
    }
}
