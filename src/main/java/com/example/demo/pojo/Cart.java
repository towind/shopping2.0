package com.example.demo.pojo;

public class Cart {
    private Integer id;
    private Integer pid;
    private Integer num;
    private double money;
    private Goods goods;
    private User user;
    
    
    public Cart() {
    	goods=new Goods();
    	user=new User();
    }

    public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Cart(int id, int pid, int num) {
        this.id = id;
        this.pid = pid;
        this.num = num;
    }


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public double getMoney() {
		return money;
	}

	public void setMoney(double money) {
		this.money = money;
	}

	public Goods getGoods() {
		return goods;
	}

	public void setGoods(Goods goods) {
		this.goods = goods;
	}

	@Override
	public String toString() {
		return "Cart [id=" + id + ", pid=" + pid + ", num=" + num + ", money="
				+ money + ", goods=" + goods + "]";
	}
    
    
}
