package com.example.demo.pojo;

import java.util.ArrayList;
import java.util.List;


public class OrderDetailView {
    private Address address;
    private Order order;
    private List<OrderDetail> orderDetails;
    
    
    
	public OrderDetailView() {
		address=new Address();
		order=new Order();
		orderDetails=new ArrayList<OrderDetail>();
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public List<OrderDetail> getOrderDetails() {
		return orderDetails;
	}
	public void setOrderDetails(List<OrderDetail> orderDetails) {
		this.orderDetails = orderDetails;
	}
	@Override
	public String toString() {
		return "OrderDetailView [address=" + address + ", order=" + order
				+ ", orderDetails=" + orderDetails + "]";
	}
    
    
}
