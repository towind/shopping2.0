package com.example.demo.service;

import com.example.demo.pojo.Order;
import com.example.demo.pojo.OrderDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderService {
    int addOrder(Order order);

    List<Order> getOrderList(Integer uid);

    //订单详情
    List<OrderDetail> getOrderDetail(String id);

    //根据订单号获取地址
    Order getOrder(String id);
}
