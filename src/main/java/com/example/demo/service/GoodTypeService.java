package com.example.demo.service;

import com.example.demo.pojo.GoodsType;

import java.util.List;

public interface GoodTypeService {

    List<GoodsType> getGoodsTypeList();
}
