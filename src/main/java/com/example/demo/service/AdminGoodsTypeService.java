package com.example.demo.service;

import com.example.demo.pojo.GoodsType;


import java.util.List;

public interface AdminGoodsTypeService {

    //查询商品类型
    List<GoodsType> getGoodsTypeList(Integer level, String name);

    //根据id删除商品类型
    int delGoodsTypeById(Integer id);
}
