package com.example.demo.service;

import com.example.demo.pojo.Cart;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CartService {
    List<Cart> getCartListById(Integer id);

    int delCartListById(Integer pid);

    int updateNum(Integer num,double money,Integer id,Integer pid);

    int delCartListAllById(Integer id);

    //加入购物车
    int addCart(Cart cart);

    //判断物品是否重复
    int checkGoods(Integer pid);

    //自加一
    int updateCartNum(Integer pid,Integer price);
}
