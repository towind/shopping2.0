package com.example.demo.service;

import com.example.demo.dto.PageInfo;
import com.example.demo.pojo.Order;
import com.example.demo.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AdminService {

    //查询所有用户信息，模糊查询用户名
    List<User> getUserListByGender(String username, String gender);

    //封禁账号
    int updUser(Integer id ,Integer flag);

    //获取所有订单
    List<Order> getOrderList(String username,String status);

    //分页合并为一个接口 用户网页界面展示
    PageInfo getPageList(String username,String status, Integer pageNo, Integer pageSize);

    int updStatus(String id );
}
