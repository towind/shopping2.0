package com.example.demo.service;

import com.example.demo.pojo.GoodsPicture;

import java.util.List;

public interface GoodsPictureService {

    List<GoodsPicture> getPicture(Integer goodsId);
}
