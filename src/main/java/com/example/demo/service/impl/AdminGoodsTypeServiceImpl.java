package com.example.demo.service.impl;

import com.example.demo.dao.AdminGoodsTypeMapper;
import com.example.demo.pojo.GoodsType;
import com.example.demo.service.AdminGoodsTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class AdminGoodsTypeServiceImpl implements AdminGoodsTypeService {

    @Resource
    private AdminGoodsTypeMapper adminGoodsTypeMapper;

    @Override
    public List<GoodsType> getGoodsTypeList(Integer level, String name) {
        return adminGoodsTypeMapper.getGoodsTypeList(level, name);
    }

    @Override
    public int delGoodsTypeById(Integer id) {
        return adminGoodsTypeMapper.delGoodsTypeById(id);
    }
}
