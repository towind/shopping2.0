package com.example.demo.service.impl;

import com.example.demo.dao.AdminMapper;
import com.example.demo.dto.PageInfo;
import com.example.demo.pojo.Order;
import com.example.demo.pojo.User;
import com.example.demo.service.AdminService;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class AdminServiceImpl implements AdminService {

    @Resource
    private AdminMapper adminMapper;

    @Override
    public List<User> getUserListByGender(String username, String gender) {
        return adminMapper.getUserListByGender(username,gender);
    }

    @Override
    public int updUser(Integer id, Integer flag) {
        return adminMapper.updUser(id, flag);
    }

    @Override
    public List<Order> getOrderList(String username, String status) {
        return null;
    }

    @Override
    public PageInfo getPageList(String username, String status, Integer pageNo, Integer pageSize) {
        PageInfo pageInfo = new PageInfo();

        int count = adminMapper.getCount(username,status);
        int offPage = (pageNo - 1) * pageSize;
        List<Order> list = adminMapper.getOrderList(username,status,offPage,pageSize);

        pageInfo.setPageSize(pageSize);
        pageInfo.setPageNo(pageNo);
        pageInfo.setMsgCount(count);
        pageInfo.setList(list);

        return pageInfo;
    }

    @Override
    public int updStatus(String id) {
        return adminMapper.updStatus(id);
    }


}
