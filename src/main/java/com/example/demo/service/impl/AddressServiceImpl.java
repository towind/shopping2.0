package com.example.demo.service.impl;

import com.example.demo.dao.AddressMapper;
import com.example.demo.pojo.Address;
import com.example.demo.service.AddressService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {
    @Resource
    private AddressMapper addressMapper;

    @Override
    public List<Address> getAddressList(Integer uid) {
        return addressMapper.getAddressList(uid);
    }

    @Override
    public int updateAddressById(Address address) {
        return addressMapper.updateAddressById(address);
    }

    @Override
    public int deleteAddressById(Integer id) {
        return addressMapper.deleteAddressById(id);
    }

    @Override
    public int updateAddressLevel(Integer id, Integer uid) {
        return addressMapper.updateAddressLevel(id, uid);
    }

    @Override
    public int addAddress(Address address) {
        return addressMapper.addAddress(address);
    }
}
