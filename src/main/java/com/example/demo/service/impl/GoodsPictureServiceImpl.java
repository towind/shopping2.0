package com.example.demo.service.impl;

import com.example.demo.dao.GoodsPictureMapper;
import com.example.demo.dto.Result;
import com.example.demo.pojo.GoodsPicture;
import com.example.demo.pojo.User;
import com.example.demo.service.GoodsPictureService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

@Service
public class GoodsPictureServiceImpl implements GoodsPictureService {
    @Resource
    private GoodsPictureMapper goodsPictureMapper;

    @Override
    public List<GoodsPicture> getPicture(Integer goodsId) {
        return goodsPictureMapper.getPicture(goodsId);
    }




}
