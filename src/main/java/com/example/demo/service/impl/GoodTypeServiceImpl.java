package com.example.demo.service.impl;

import com.example.demo.dao.GoodTypeMapper;
import com.example.demo.pojo.GoodsType;
import com.example.demo.service.GoodTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class GoodTypeServiceImpl implements GoodTypeService {

    @Resource
    private GoodTypeMapper goodTypeMapper;


    @Override
    public List<GoodsType> getGoodsTypeList() {
        return goodTypeMapper.getGoodsTypeList();
    }
}
