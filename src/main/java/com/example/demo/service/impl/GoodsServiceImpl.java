package com.example.demo.service.impl;

import com.example.demo.dao.GoodsMapper;
import com.example.demo.dto.PageInfo;
import com.example.demo.pojo.Goods;
import com.example.demo.service.GoodsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class GoodsServiceImpl implements GoodsService {
    @Resource
    private GoodsMapper goodsMapper;

    @Override
    public PageInfo getPageInfo(String name, String typeId, Integer pageNo) {
        int ofPage=(pageNo-1)*8;

        int count = goodsMapper.countMsgByName(name,typeId);
        List<Goods> list = goodsMapper.getGoodsByName(name,typeId,ofPage);

        PageInfo info=new PageInfo();

        info.setPageSize(8);
        info.setMsgCount(count);
        info.setPageNo(pageNo);
        info.setList(list);
        return info;
    }

    @Override
    public Goods getGoodsById(Integer id) {
        return goodsMapper.getGoodsById(id);
    }


}
