package com.example.demo.service.impl;

import com.example.demo.dao.UserMapper;
import com.example.demo.dto.Result;
import com.example.demo.pojo.User;
import com.example.demo.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserMapper userMapper;


    @Override
    public int addUser(User user) {
        user.setRole(1);
        user.setFlag(1);
        return userMapper.addUser(user);
    }

    @Override
    public User getUser(String username) {
        return userMapper.getUserByName(username);
    }

    @Override
    public Result getUserByName(String username, String password) {
        User user = userMapper.getUserByName(username);
        if (user!=null){
            if (user.getFlag()==1 && user.getRole()==1){
                if (user.getPassword().equals(password)){
                return Result.success(user);
                }
                return Result.error("1","密码错误");
            }else {
                return Result.error("1","该账号已冻结！");
            }
        }
        return Result.error("1","该用户名不存在!");
    }

    @Override
    public Result getUserBypassword(String username, String password) {

        User user = userMapper.getUserByName(username);
        if(user!=null){
            if(user.getFlag()==1 && user.getRole()==0){
                if(user.getPassword().equals(password)){
                    return Result.success(user);
                }
                return Result.error("1","密码错误!");
            }else{
                return Result.error("1","该账号已冻结！");
            }
        }
        return Result.error("1","该用户名不存在!");
    }
}
