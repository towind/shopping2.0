package com.example.demo.service.impl;

import com.example.demo.dao.AdminGoodsMapper;
import com.example.demo.dao.GoodTypeMapper;
import com.example.demo.dao.GoodsMapper;
import com.example.demo.dto.PageInfo;
import com.example.demo.pojo.Goods;
import com.example.demo.service.AdminGoodsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class AdminGoodsServiceImpl implements AdminGoodsService {

    @Resource
    private AdminGoodsMapper adminGoodsMapper;

    @Resource
    private GoodTypeMapper goodTypeMapper;

    @Override
    public PageInfo getGoodsPageList(String name, Date pubdate, Integer pageNo) {
        PageInfo pageInfo = new PageInfo();

        int count = adminGoodsMapper.getGoodsCount(name, pubdate);
        int ofPage=(pageNo-1)*8;
        List<Goods> list = adminGoodsMapper.getGoodsPageList(name, pubdate, ofPage);
        for (Goods goods : list) {
            goods.setGoodsType(goodTypeMapper.getGoodTypeById(goods.getTypeId()));

        }

        pageInfo.setPageSize(8);
        pageInfo.setMsgCount(count);
        pageInfo.setPageNo(pageNo);
        pageInfo.setList(list);
        return pageInfo;
    }

    @Override
    public int addGoods(Goods goods) {
        return adminGoodsMapper.addGoods(goods);
    }

    @Override
    public int deleteGoods(String id) {
        return adminGoodsMapper.deleteGoods(id);
    }

    @Override
    public int updateGoods(Goods goods) {
        return adminGoodsMapper.updateGoods(goods);
    }

    @Override
    public Goods getGoodsById(String id) {
        return adminGoodsMapper.getGoodsById(id);
    }
}
