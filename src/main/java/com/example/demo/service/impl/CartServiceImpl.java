package com.example.demo.service.impl;

import com.example.demo.dao.CartMapper;
import com.example.demo.pojo.Cart;
import com.example.demo.service.CartService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class CartServiceImpl implements CartService {
    @Resource
    private CartMapper cartMapper;
    @Override
    public List<Cart> getCartListById(Integer id) {
        return cartMapper.getCartListById(id);
    }

    @Override
    public int delCartListById(Integer pid) {
        return cartMapper.delCartListById(pid);
    }

    @Override
    public int updateNum(Integer num, double money, Integer id, Integer pid) {
        return cartMapper.updateNum(num,money,id,pid);
    }

    @Override
    public int delCartListAllById(Integer id) {
        return cartMapper.delCartListAllById(id);
    }

    @Override
    public int addCart(Cart cart) {
        return cartMapper.addCart(cart);
    }

    @Override
    public int checkGoods(Integer pid) {
        return cartMapper.checkGoods(pid);
    }

    @Override
    public int updateCartNum(Integer pid, Integer price) {
        return cartMapper.updateCartNum(pid,price);
    }
}
