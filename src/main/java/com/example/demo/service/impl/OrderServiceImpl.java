package com.example.demo.service.impl;

import com.example.demo.dao.OrderMapper;
import com.example.demo.pojo.Order;
import com.example.demo.pojo.OrderDetail;
import com.example.demo.service.OrderService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Resource
    private OrderMapper orderMapper;
    @Override
    public int addOrder(Order order) {
        return orderMapper.addOrder(order);
    }

    @Override
    public List<Order> getOrderList(Integer uid) {
        return orderMapper.getOrderList(uid);
    }

    @Override
    public List<OrderDetail> getOrderDetail(String id) {
        return orderMapper.getOrderDetail(id);
    }

    @Override
    public Order getOrder(String oid) {
        return orderMapper.getOrder(oid);
    }
}
