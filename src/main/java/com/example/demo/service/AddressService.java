package com.example.demo.service;

import com.example.demo.pojo.Address;


import java.util.List;

public interface AddressService {

    List<Address> getAddressList(Integer uid);

    int updateAddressById(Address address);

    int deleteAddressById(Integer id);

    int updateAddressLevel(Integer id,Integer uid);

    int addAddress(Address address);
}
