package com.example.demo.service;

import com.example.demo.dto.PageInfo;
import com.example.demo.pojo.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.Date;


public interface AdminGoodsService {

    //获取所有商品
    PageInfo getGoodsPageList(String name, Date pubdate, Integer pageNo);

    //添加商品
    int addGoods(Goods goods);

    //删除商品
    int deleteGoods(String id);

    //更改商品
    int updateGoods(Goods goods);

    //根据id查询商品
    Goods getGoodsById(String id);

}
