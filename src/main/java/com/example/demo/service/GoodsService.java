package com.example.demo.service;

import com.example.demo.dto.PageInfo;
import com.example.demo.pojo.Goods;

public interface GoodsService {

    PageInfo getPageInfo(String name, String typeId, Integer pageNo);

    Goods getGoodsById(Integer id);

}
