package com.example.demo.service;

import com.example.demo.dto.Result;
import com.example.demo.pojo.User;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import com.example.demo.pojo.User;

public interface UserService {

    //注册功能
    int addUser(User user);

    User getUser(String username);

    //登录功能
    Result getUserByName(String username, String password);

    Result getUserBypassword(String username,String password);

}
