package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TestCode {

    @RequestMapping("/toDetails")
    public String toDetails(){
        return "details";
    }

    @RequestMapping("/toShowAll")
    public String toShowAll(){
        return "admin/showAll";
    }

    @RequestMapping("/toAdmin")
    public String toAdminLogin(){
        return "admin/login";
    }

//    @RequestMapping("/toUserList")
//    public String toUserList(){
//        return "admin/userList";
//    }

//    @RequestMapping("/toShowGoodsType")
//    public String toShowGoodsType(){
//        return "admin/showGoodsType";
//    }

    @RequestMapping("/toAddGoodsType")
    public String toAddGoodsType(){
        return "admin/addGoodsType";
    }

//    @RequestMapping("/toShowGoods")
//    public String toShowGoods(){
//        return "admin/showGoods";
//    }

//    @RequestMapping("/toAddGoods")
//    public String toAddGoods(){
//        return "admin/addGoods";
//    }

    @RequestMapping("/toShowAllOrder")
    public String toShowAllOrder(){
        return "admin/showAllOrder";
    }
}
