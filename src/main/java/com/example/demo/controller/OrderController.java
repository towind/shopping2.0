package com.example.demo.controller;

import com.example.demo.dto.OrderRand;
import com.example.demo.dto.Result;
import com.example.demo.pojo.Order;
import com.example.demo.pojo.OrderDetail;
import com.example.demo.pojo.User;
import com.example.demo.service.OrderService;
import com.sun.org.apache.xpath.internal.operations.Or;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

@Controller
public class OrderController {
    @Resource
    private OrderService orderService;

    //新增一条订单，跳转到购物车清空购物车
    @RequestMapping("/addOrder")
     public String addOrder(@RequestParam(value = "money",defaultValue = "0",required = false)Double money,
                            @RequestParam(value = "aid",defaultValue = "0",required = false)Integer aid,
                            HttpSession session) {
        String str = OrderRand.getOrder(32);
        User user = (User) session.getAttribute("user");
        System.out.println("money======="+money);
        System.out.println("aid======="+aid);
        Order order = new Order();

        order.setId(str);
        order.setUid(user.getId());
        order.setMoney(money);
        order.setStatus("1");
        order.setTime(new Date());
        order.setAid(aid);
        int count=orderService.addOrder(order);
        if (count==1){
            session.setAttribute("order",order);
            return "redirect:/addOrderDelAllCart";
        }
    return "order";
    }
    //获取我的订单信息到我的订单
    @RequestMapping("/getOrder")
    public String getOrder(HttpSession session,Model model){
        User user = (User) session.getAttribute("user");
        List<Order> orderList=orderService.getOrderList(user.getId());
        model.addAttribute("orderList",orderList);
        return "orderList";
    }


    @RequestMapping("/getOrderByOid")
    public String getOrderByOid(@RequestParam(value = "id",defaultValue = "",required = false)String id,HttpSession session){
        Order order=orderService.getOrder(id);
        System.out.println(order.getId());
        session.setAttribute("order",order);
        return "redirect:/getOrderDetail";
    }

    @RequestMapping("/getOrderDetail")
    public String getOrderDetail(HttpSession session){
        Order order = (Order) session.getAttribute("order");
        System.out.println(order.toString());
        List<OrderDetail> getOrderDetailList=orderService.getOrderDetail(order.getId());
        System.out.println(Result.success(getOrderDetailList));
        session.setAttribute("getOrderDetailList",getOrderDetailList);
        return "orderDetail";
    }

}
