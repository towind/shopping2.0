package com.example.demo.controller;

import com.example.demo.dto.Result;
import com.example.demo.pojo.GoodsPicture;
import com.example.demo.pojo.User;
import com.example.demo.service.GoodsPictureService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class GoodsPictureController {
    @Resource
    private GoodsPictureService goodsPictureService;

    @ResponseBody
    @RequestMapping("/getPicture")
    public Result getPicture(HttpSession session){
        Integer id = Integer.parseInt(session.getAttribute("id").toString());
        List<GoodsPicture> list = goodsPictureService.getPicture(id);
        System.out.println(Result.success(list));
        return Result.success(list);
    }
}
