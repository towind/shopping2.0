package com.example.demo.controller;

import com.example.demo.dto.Result;
import com.example.demo.pojo.Cart;
import com.example.demo.pojo.User;
import com.example.demo.service.CartService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class CartController {

    @Resource
    private CartService cartService;


    //带用户id获取购物车信息
    @RequestMapping("/goCart")
    public String goCart(HttpSession session,@RequestParam("id")Integer id){
        System.out.println("============++========="+id);

        session.setAttribute("id",id);
        return "redirect:/cart";
    }



    //跳转到购物车界面
    @RequestMapping("/cart")
    public String cart(HttpSession session, Model model){
        List<Cart> cartList=cartService.getCartListById(Integer.parseInt(session.getAttribute("id").toString()));
        System.out.println("===================="+Integer.parseInt(session.getAttribute("id").toString()));
        System.out.println(cartList.size());
        if (cartList!=null&&cartList.size()>0){
            model.addAttribute("cartList",cartList);
            return "cart";
        }
        return "redirect:/toCart";
    }



    //移除购物车商品
    @RequestMapping("/delCart")
    public String delCart(@RequestParam(value = "pid")Integer pid){
        int count=cartService.delCartListById(pid);
        if (count==1){
            return "redirect:/cart";
        }
        return "redirect:/cart";
    }

    //修改购物车商品数量和价格
    @ResponseBody
    @RequestMapping("/updNum")
    public Result updNum(@RequestParam(value = "num",defaultValue = "0",required = false)Integer num,
                         @RequestParam(value = "money",defaultValue = "0",required = false)Double money,
                         @RequestParam(value = "id",defaultValue = "0",required = false)Integer id,
                         @RequestParam(value = "pid",defaultValue = "0",required = false)Integer pid){
        int count=cartService.updateNum(num,money,id,pid);

        if (count==1){
            return Result.error("0","成功");
        }
        return Result.success("1","失败");

    }


    //购物车没数据去到的界面
    @RequestMapping("/toCart")
    public String toCart(HttpSession session){
        session.setAttribute("cart",null);
        return "emtryCarts";
    }


    //提交订单后移除购物车所有商品
    @RequestMapping("/addOrderDelAllCart")
    public String addOrderDelAllCart(HttpSession session){
        User user = (User) session.getAttribute("user");
        int count=cartService.delCartListAllById(user.getId());
        if (count>=1){
            return "orderSuccess";
        }
        return "order";
    }

    @RequestMapping("/delAllCart")
    public String delAllCart(HttpSession session){
        User user = (User) session.getAttribute("user");
        int count=cartService.delCartListAllById(user.getId());
        if (count>=1){
            return "emtryCarts";
        }
        return "redirect:/cart";
    }

    //获取购物车数据
    @ResponseBody
    @RequestMapping("/getCartList")
    public Result getCartList(HttpSession session){
        User user = (User) session.getAttribute("user");
        List<Cart> cartList=cartService.getCartListById(user.getId());
        System.out.println(Result.success(cartList));
        return Result.success(cartList);
    }

    //去到订单预览界面
    @RequestMapping("/toOrder")
    public String toOrder(){
        return "order";
    }


    @ResponseBody
    @RequestMapping("/toAddCart")
    public Result toAddCart(@RequestParam(value = "id")Integer id,
                            @RequestParam(value = "money")Integer money,
                            HttpSession session){
        int flag=0;
        User user = (User) session.getAttribute("user");
        if (user==null){
            return Result.success("-1");
        }
        int count = cartService.checkGoods(id);
        if (count>0){
            flag = cartService.updateCartNum(id, money);
        }else{
            Cart cart=new Cart();
            cart.setId(user.getId());
            cart.setMoney(money);
            cart.setPid(id);
            cart.setNum(1);
            flag = cartService.addCart(cart);
        }
        return Result.success(flag);
    }

}
