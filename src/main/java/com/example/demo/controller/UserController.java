package com.example.demo.controller;

import com.example.demo.dto.Result;
import com.example.demo.pojo.User;
import com.example.demo.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

@Controller
public class UserController {

    @Resource
    private UserService userService;

    @RequestMapping("/")//首页路径
    public String toIndex(){
        return "index";
    }

    @RequestMapping("/toUser")//去往注册页面
    public String toUser(){
        return "register";
    }

    @RequestMapping("/regSuc")//去往注册成功页面
    public String regSuc(){
        return "registerSuccess";
    }

    @RequestMapping("/toExit")//注销功能
    public String toExit(HttpSession session){
        session.removeAttribute("user");
        return "index";
    }


    @ResponseBody
    @RequestMapping("/toAdd")//执行添加用户  注册功能
    public void toAdd(@RequestParam("username")String username,
                      @RequestParam("password")String password,
                      @RequestParam("email")String email,
                      @RequestParam("gender")String gender,
                      HttpServletResponse response) throws IOException {
        response.setContentType("text/html;charset=utf-8");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
         User user=new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);
        user.setGender(gender);
        int count = userService.addUser(user);
        out.print(count==1?true:false);
    }


    @RequestMapping("/toLogin")//去往登录页面
    public String toLogin(){
        return "login";
    }

    @ResponseBody
    @RequestMapping("/login")//登录方法
    public Result login(@RequestParam("username")String username,@RequestParam("password")String password, Model model, HttpSession session){
        Result result = userService.getUserByName(username, password);
        if (result.getCode().equals("0")){
            session.setAttribute("user",result.getData());
        }
        System.out.println(result);
        return result;
    }

    @ResponseBody
    @RequestMapping("/cation")
    public void cation(@RequestParam("username")String username,HttpServletResponse response) throws IOException {
        User user = userService.getUser(username);
        PrintWriter out = response.getWriter();
        out.print(user!=null?true:false);
    }



    @ResponseBody
    @RequestMapping("/toAdminLogin")
    public Result toAdminLogin(User user,HttpSession session){
        Result result = userService.getUserBypassword(user.getUsername(), user.getPassword());
        if (result.getCode().equals("0")){
            session.setAttribute("admin",result.getData());
        }
        System.out.println(result);
        return result;
    }
}
