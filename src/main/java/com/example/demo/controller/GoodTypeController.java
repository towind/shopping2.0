package com.example.demo.controller;

import com.example.demo.dao.GoodTypeMapper;
import com.example.demo.dto.Result;
import com.example.demo.pojo.GoodsType;
import com.example.demo.service.GoodTypeService;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class GoodTypeController {

    @Resource
    private GoodTypeService goodTypeService;

    @ResponseBody
    @RequestMapping("/getGoodType")
    public Result getGoodType(){
        List<GoodsType> list = goodTypeService.getGoodsTypeList();
        System.out.println(Result.success(list));
        return Result.success(list);
    }


}
