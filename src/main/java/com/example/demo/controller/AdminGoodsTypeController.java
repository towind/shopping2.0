package com.example.demo.controller;

import com.example.demo.dto.Result;
import com.example.demo.pojo.GoodsType;
import com.example.demo.service.AdminGoodsTypeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class AdminGoodsTypeController {

    @Resource
    private AdminGoodsTypeService adminGoodsTypeService;



    @RequestMapping("/toShowGoodsType")
    public String toShowGoodsType(@RequestParam(value = "level", defaultValue = "")Integer level,
                                  @RequestParam(value = "name",defaultValue = "") String name,Model model){

        System.out.println("level"+level+"name"+name);
        List<GoodsType> goodsTypeList = adminGoodsTypeService.getGoodsTypeList(level, name);
//        System.out.println(goodsTypeList);
        model.addAttribute("goodsTypeList",goodsTypeList);
        return "admin/showGoodsType";
    }

    @ResponseBody
    @RequestMapping("/goToShowGoodsType")
    public Result goToShowGoodsType(@RequestParam(value = "level", defaultValue = "")Integer level,
                                    @RequestParam(value = "name",defaultValue = "") String name){
        List<GoodsType> goodsTypeList = adminGoodsTypeService.getGoodsTypeList(level, name);
        System.out.println(Result.success(goodsTypeList));
        return Result.success(goodsTypeList);
    }

    @ResponseBody
    @RequestMapping("/delGoodsTypeById")
    public Result delGoodsTypeById(@RequestParam("id")Integer id){

        int count = adminGoodsTypeService.delGoodsTypeById(id);
        if (count==1){
            return Result.success("0");
        }
           return Result.error("1","");
    }
}
