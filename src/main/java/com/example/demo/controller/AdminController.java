package com.example.demo.controller;

import com.example.demo.dto.PageInfo;
import com.example.demo.dto.Result;
import com.example.demo.pojo.Order;
import com.example.demo.pojo.User;
import com.example.demo.service.AdminService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class AdminController {

    @Resource
    private AdminService adminService;

    @RequestMapping("/toUserList")
    public String toUserList(){
        return "admin/userList";
    }

    @ResponseBody
    @RequestMapping("/getUserList")
    public Result getUserList(@RequestParam(value = "username",required = false,defaultValue = "")String username,
                           @RequestParam(value = "gender",required = false,defaultValue = "")String gender){
        List<User> userList = adminService.getUserListByGender(username, gender);
        System.out.println(Result.success(userList));

        return Result.success(userList);
    }
    
    @ResponseBody
    @RequestMapping("/updUserFlag")
    public Result updUserFlag(@RequestParam("id")Integer id,
                              @RequestParam("flag")Integer flag){
        int count = adminService.updUser(id, flag);
        if (count==1){
            return Result.success("0","");
        }
        return Result.error("1","");
    }

    //获取订单列表信息
    @ResponseBody
   @RequestMapping("/getShowAllOrder")
    public Result getShowAllOrder(@RequestParam(value = "username",required = false,defaultValue = "")String username,
                                 @RequestParam(value = "status",required = false,defaultValue = "")String status,
                                 @RequestParam(value = "pageNo",required = false,defaultValue = "1")Integer pageNo,
                                 @RequestParam(value = "pageSize",required = false,defaultValue = "10")Integer pageSize
                                 ){
       PageInfo pageInfo=adminService.getPageList(username,status,pageNo,pageSize);
       System.out.println(Result.success(pageInfo));
       return Result.success(pageInfo);
   }
    //跳转到订单列表页面
    @RequestMapping("/goShowAllOrder")
    public String goShowAllOrder(){
        return "admin/showAllOrder";
    }

    @ResponseBody
    @RequestMapping("/updStatus")
    public Result updStatus(@RequestParam(value = "id",required = false,defaultValue = "")String id){
        int count=adminService.updStatus(id);
        if (count==1){
            return Result.success("0","成功发货");
        }
        return Result.error("1","发货失败");
    }
    
}
