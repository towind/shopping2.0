package com.example.demo.controller;


import com.example.demo.dto.PageInfo;
import com.example.demo.dto.Result;
import com.example.demo.pojo.Goods;
import com.example.demo.pojo.GoodsType;
import com.example.demo.service.AdminGoodsService;
import com.example.demo.service.GoodTypeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class AdminGoodsController {

    @Resource
    private AdminGoodsService adminGoodsService;

    @Resource
    private GoodTypeService goodTypeService;

    @RequestMapping("/toShowGoods")
    public String toShowGoods(@RequestParam(value = "name",defaultValue = "")String name,
                              @RequestParam(value = "pubdate",defaultValue = "") String pubdate,
                              Model model,@RequestParam(value = "pageNo",defaultValue = "1")Integer pageNo) throws ParseException {
        Date parse=null;
       if (!pubdate.equals("")){
           SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
           parse = sdf.parse(pubdate);
       }

        PageInfo info = adminGoodsService.getGoodsPageList(name, parse,pageNo);
//        System.out.println(info);
        model.addAttribute("info",info);

        return "admin/showGoods";
    }

    @ResponseBody
    @RequestMapping("/toShowGoodsPage")
    public Result toPage(@RequestParam(value = "name",defaultValue = "")String name,
                         @RequestParam(value = "pubdate",defaultValue = "")String pubdate,
                         @RequestParam(value = "pageNo",defaultValue = "1")Integer pageNo) throws ParseException{
        Date parse=null;
        if (!pubdate.equals("")){
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
            parse = sdf.parse(pubdate);
        }
        PageInfo info =adminGoodsService.getGoodsPageList(name, parse,pageNo);
        System.out.println(Result.success(info));
        return Result.success(info);
    }

    @ResponseBody
    @RequestMapping("/delGoods")
    public Result delGoods(@RequestParam("id")String id){
        int count = adminGoodsService.deleteGoods(id);
        if (count==1){
            return Result.success("0","");
        }
        return Result.error("1","");

    }

    @ResponseBody
    @RequestMapping("/updGoodsById")
    public Result updGoodsById(@RequestParam("id")String id,
                               @RequestParam("name")String name,
                               @RequestParam("price")Double price,
                               @RequestParam("intro")String intro){
        Goods goods = adminGoodsService.getGoodsById(id);
        goods.setName(name);
        goods.setPrice(price);
        goods.setIntro(intro);

        int count = adminGoodsService.updateGoods(goods);
        if (count==1){
            return Result.success("0","更改商品信息成功！");
        }
        return Result.error("1","更改商品信息失败！");

    }

    @RequestMapping("/toAddGoods")
    public String toAddGoods(Model model){
        List<GoodsType> goodsTypeList = goodTypeService.getGoodsTypeList();
        model.addAttribute("goodsTypeList",goodsTypeList);
        return "admin/addGoods";
    }

    @ResponseBody
    @RequestMapping("/AddGoods")
    private Result addGoods(@RequestParam(value = "name",required = false,defaultValue = "")String name,
                            @RequestParam(value = "typeid",required = false,defaultValue = "") Integer typeid,
                            @RequestParam(value = "pubdate",required = false,defaultValue = "")String pubdate,
                            @RequestParam(value = "price",required = false,defaultValue = "") Double price,
                            @RequestParam(value = "star",required = false,defaultValue = "") Integer star,
                            @RequestParam(value = "picture",required = false,defaultValue = "") String picture,
                            @RequestParam(value = "intro",required = false,defaultValue = "") String intro) throws ParseException {
        Goods goods=new Goods();
        goods.setIntro(intro);
        goods.setName(name);
        goods.setPicture(picture);
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        Date parse = sdf.parse(pubdate);
        System.out.println(parse);
        goods.setPubdate(parse);
        goods.setStar(star);
        goods.setTypeId(typeid);
        goods.setPrice(price);

        int count = adminGoodsService.addGoods(goods);
        if (count==1){
            return Result.success("0","添加商品成功！");
        }

        return Result.error("1","添加商品失败!");

    }

}
