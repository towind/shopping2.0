package com.example.demo.controller;

import com.example.demo.dto.Result;
import com.example.demo.pojo.Address;
import com.example.demo.pojo.User;
import com.example.demo.service.AddressService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class AddressController {

    @Resource
    private AddressService addressService;

    //带用户id获取用户地址
    @RequestMapping("/ToAddress")
    public String goCart(HttpSession session, @RequestParam("id")Integer id){
        session.setAttribute("id",id);
        return "redirect:/Address";
    }

    //跳转到用户地址界面
    @RequestMapping("/Address")
    public String ToAddress(HttpSession session, Model model){
        List<Address> addressList = addressService.getAddressList(Integer.parseInt(session.getAttribute("id").toString()));
        model.addAttribute("addressList",addressList);

        return "self_info";
    }

    //拿到地址数据
    @ResponseBody
    @RequestMapping("/getAddressList")
    public Result getAddressList(HttpSession session){
        User user = (User) session.getAttribute("user");
        List<Address> addressList = addressService.getAddressList(user.getId());
        System.out.println(Result.success(addressList));
        return Result.success(addressList);
    }
//    @RequestMapping("AddAddress")
//    public


    //添加地址
    @ResponseBody
    @RequestMapping("/addAddress")
    public Result addAddress(@RequestParam("name")String name,
                             @RequestParam("phone")String phone,
                             @RequestParam("detail")String detail,
                             @RequestParam("uid")Integer uid
                             ){
        Address address=new Address();
        address.setDetail(detail);
        address.setName(name);
        address.setPhone(phone);
        address.setUid(uid);
        addressService.updateAddressLevel(null, uid);

        int count = addressService.addAddress(address);
        if (count==1){
            return Result.success("0","添加地址成功！");
        }
        return Result.error("1","添加地址失败！");
    }

    //删除地址
    @ResponseBody
    @RequestMapping("/deleteAddress")
    public Result deleteAddress(@RequestParam("id")Integer id,HttpSession session){
        int count = addressService.deleteAddressById(id);
        User user = (User) session.getAttribute("user");
        if (count==1){
            List<Address> list = addressService.getAddressList(user.getId());
            addressService.updateAddressLevel(null,user.getId());
            addressService.updateAddressLevel(list.get(0).getId(),null);
            return Result.success("0","删除地址成功！");
        }
        return Result.error("1","删除地址失败！");
    }

    //更改地址
    @ResponseBody
    @RequestMapping("/updAddress")
    public Result updAddress(@RequestParam("id")Integer id,
                             @RequestParam("updUid")Integer updUid,
                             @RequestParam(value = "updName",required = false, defaultValue = "")String updName,
                             @RequestParam(value = "updPhone",required = false, defaultValue = "")String updPhone,
                             @RequestParam(value = "updDetail",required = false, defaultValue = "")String updDetail){

        Address address=new Address();
        address.setId(id);
        address.setUid(updUid);
        address.setDetail(updDetail);
        address.setName(updName);
        address.setPhone(updPhone);
        System.out.println("要修改的address===》"+address.toString());
        int count = addressService.updateAddressById(address);
        if (count==1){

            return Result.success("0","更改地址成功！");

        }
        return Result.error("1","更改地址失败！");
    }

    @ResponseBody
    @RequestMapping("/defaultAddr")
    public Result defaultAddr(@RequestParam("id")Integer id,
                              HttpSession session){
        User user = (User) session.getAttribute("user");
        int count = addressService.updateAddressLevel(null, user.getId());
        int count1=addressService.updateAddressLevel(id,null);
        if(count==1||count1==1){
            return Result.success("0","设为默认成功！");

        }
        return Result.error("1","设为默认失败！");
    }

}
