package com.example.demo.controller;

import com.example.demo.dto.PageInfo;
import com.example.demo.dto.Result;
import com.example.demo.pojo.Goods;
import com.example.demo.service.GoodsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

@Controller
public class GoodsController {

    @Resource
    private GoodsService goodsService;

    @RequestMapping("/toSearch")
    public String toSearch(@RequestParam(value = "name",defaultValue = "")String name,
                           @RequestParam(value = "typeId",defaultValue = "")String typeId,
                           Model model, @RequestParam(value = "pageNo",defaultValue = "1")Integer pageNo){
        PageInfo info = goodsService.getPageInfo(name,typeId, pageNo);
        System.out.println(info);
        model.addAttribute("info",info);
        model.addAttribute("typeId",typeId);
        model.addAttribute("name",name);
        return "goodsList";
    }


    @ResponseBody
    @RequestMapping("/toPage")
    public Result toPage(@RequestParam(value = "name",defaultValue = "")String name,
                         @RequestParam(value = "typeId",defaultValue = "")String typeId,
                         @RequestParam(value = "pageNo",defaultValue = "1")Integer pageNo){
        PageInfo info = goodsService.getPageInfo(name,typeId, pageNo);
        System.out.println(Result.success(info));
        return Result.success(info);
    }

    @RequestMapping("/toShow")
    public String toShow (@RequestParam("id")Integer id, Model model, HttpSession session){
        Goods goods = goodsService.getGoodsById(id);
        session.setAttribute("id",id);
        System.out.println(goods.toString());
        model.addAttribute("goods",goods);
        return "details";
    }

}
