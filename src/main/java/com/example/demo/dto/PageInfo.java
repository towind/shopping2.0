package com.example.demo.dto;

import java.util.List;

public class PageInfo<T> {
	//获取所有信息总条数
	private Integer msgCount;
	//获取当前页码
	private Integer pageNo;
	//获取页面显示行数
	private Integer pageSize;
	//获取总页数
	private Integer pageCount;
	//获取当前页显示集合
	private List<T> list;

	public Integer getMsgCount() {
		return msgCount;
	}

	public void setMsgCount(Integer msgCount) {
		if(msgCount>0){
			this.msgCount = msgCount;
			this.pageCount=this.msgCount%this.pageSize!=0?(this.msgCount/this.pageSize)+1:(this.msgCount/this.pageSize);
		}

	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getPageCount() {
		return pageCount;
	}

    /*public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }*/

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "PageInfo{" +
				"msgCount=" + msgCount +
				", pageNo=" + pageNo +
				", pageSize=" + pageSize +
				", pageCount=" + pageCount +
				", list=" + list +
				'}';
	}
}