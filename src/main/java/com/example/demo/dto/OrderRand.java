package com.example.demo.dto;

import java.util.Random;



public class  OrderRand {
	
	public static String getOrder(int length){
		String base="0123456789abcdef";
		Random random=new Random();
		StringBuffer sb=new StringBuffer();
		for (int i = 0; i < length; i++) {
			int number=random.nextInt(base.length()-1);
			sb.append(base.charAt(number));
		}
		return sb.toString();
	}
	
	public static void main(String[] args) {
		System.out.println(OrderRand.getOrder(32));
	}
	

}
