package com.example.demo.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import com.example.demo.pojo.Address;

import java.util.List;

@Mapper
public interface AddressMapper {

    List<Address> getAddressList(@Param("uid") Integer uid);

    int updateAddressById(Address address);

    int deleteAddressById(@Param("id")Integer id);

    int updateAddressLevel(@Param("id")Integer id,@Param("uid")Integer uid);

    int addAddress(Address address);
}
