package com.example.demo.dao;

import com.example.demo.pojo.GoodsType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface GoodTypeMapper {

    List<GoodsType> getGoodsTypeList();

    GoodsType getGoodTypeById(@Param("id")Integer id);

}
