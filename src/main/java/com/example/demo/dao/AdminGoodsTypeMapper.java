package com.example.demo.dao;

import com.example.demo.pojo.GoodsType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AdminGoodsTypeMapper {

    //查询商品类型
    List<GoodsType> getGoodsTypeList(@Param("level")Integer level,@Param("name")String name);

    //根据id删除商品类型
    int delGoodsTypeById(@Param("id")Integer id);
}
