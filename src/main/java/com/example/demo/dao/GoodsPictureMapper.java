package com.example.demo.dao;

import com.example.demo.pojo.GoodsPicture;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface GoodsPictureMapper {

    List<GoodsPicture> getPicture(@Param("goodsId")Integer goodsId);
}
