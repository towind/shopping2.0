package com.example.demo.dao;

import com.example.demo.pojo.Cart;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface CartMapper {
    //根据用户id获取购物车信息
    List<Cart> getCartListById(@Param("id") Integer id);

    //移除购物车里的商品
    int delCartListById(@Param("pid") Integer pid);

    //移除购物车里所有的商品
    int delCartListAllById(@Param("id") Integer id);

    //更新商品数量
    int updateNum(@Param("num")Integer num,@Param("money")double money,@Param("id")Integer id,@Param("pid")Integer pid);

    //加入购物车
    int addCart(Cart cart);

    //判断物品是否重复
    int checkGoods(@Param("pid")Integer pid);

    //自加一
    int updateCartNum(@Param("id")Integer pid,@Param("price")Integer price);


}
