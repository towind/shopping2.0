package com.example.demo.dao;

import com.example.demo.pojo.Goods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface AdminGoodsMapper {


    int getGoodsCount(@Param("name") String name, @Param("pubdate") Date pubdate);

    //获取所有商品
    List<Goods> getGoodsPageList(@Param("name")String name,@Param("pubdate")Date pubdate,@Param("pageNo")Integer pageNo);

    //添加商品
    int addGoods(Goods goods);

    //删除商品
    int deleteGoods(String id);

    //更改商品
    int updateGoods(Goods goods);

    //根据id查询商品
    Goods getGoodsById(@Param("id") String id);
}
