package com.example.demo.dao;

import com.example.demo.pojo.Goods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface GoodsMapper {

    List<Goods> getGoodsByName(@Param("name")String name,
                               @Param("typeId")String typeId,
                               @Param("pageNo")Integer pageNo);

    int countMsgByName(@Param("name")String name,@Param("typeId")String typeId);

    Goods getGoodsById(@Param("id")Integer id);


}
