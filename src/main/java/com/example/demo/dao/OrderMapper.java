package com.example.demo.dao;

import com.example.demo.pojo.Order;
import com.example.demo.pojo.OrderDetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrderMapper {
    //提交订单
    int addOrder(Order order);


    //获取我的订单信息
    List<Order> getOrderList(@Param("uid") Integer uid);

    //订单详情
    List<OrderDetail> getOrderDetail(@Param("id")String id);

    //根据订单号获取地址
    Order getOrder(@Param("id")String id);
}
