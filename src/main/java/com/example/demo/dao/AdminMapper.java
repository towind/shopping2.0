package com.example.demo.dao;

import com.example.demo.pojo.Order;
import com.example.demo.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AdminMapper {

    //查询所有用户信息，模糊查询用户名
    List<User> getUserListByGender(@Param("username")String username,@Param("gender")String gender);

    //封禁账号
    int updUser(@Param("id")Integer id ,@Param("flag")Integer flag);

    //分类
    //总数量 确定总页码
    int getCount(@Param("username") String username,@Param("status")String status);


    //获取所有订单
    List<Order> getOrderList(@Param("username") String username,@Param("status")String status,@Param("pageNo")Integer pageNo,@Param("pageSize")Integer pageSize);

    int updStatus(@Param("id")String id);
    //修改订单状态



}
