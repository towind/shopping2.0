<%--
  Created by IntelliJ IDEA.
  User: Gosick
  Date: 2022/4/8
  Time: 8:36
  To change this template use File | Settings | File Templates.
--%>
<%--<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>--%>
<%@ page  language="java" pageEncoding="UTF-8"   %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8"/>
    <title>小米商城</title>
    <link rel="icon" href="img/小米手机/logo-footer.png" type="image/gif">
    <link rel="stylesheet" href="css/layui/css/layui.css">
    <link href="css/jyc.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="css/iconfont.css">

    <script src="css/layui/layui.js"></script>

    <script src="js/jquery-3.3.1.js"></script>
    <script src="js/jyc.js"></script>
    <link rel="stylesheet" href="css/index.css">
    <link rel="icon" href="img/小米手机/logo-footer.png" type="image/gif" >
    <link rel="stylesheet" href="css/layui/css/layui.css">
    <link href="css/jyc.css" rel="stylesheet"  type="text/css">
    <link rel="stylesheet" type="text/css" href="css/iconfont.css">

    <script src="css/layui/layui.js"></script>

    <script src="js/jquery-3.3.1.js"></script>
    <link rel="stylesheet" href="css/index.css">
    <script type="text/javascript">
        $(function () {
            layui.use('carousel', function(){
                var carousel = layui.carousel;
                //建造实例
                carousel.render({
                    elem: '#home-carousel'
                    ,width: '100%' //设置容器宽度
                    ,height: '460px'
                    ,arrow: 'always' //始终显示箭头
                    //,anim: 'updown' //切换动画方式

                });
            });
        })
    </script>

    <style>
        .nav-search{
            float:right;
            margin-top:25px;
            width:297px;
        }
        .shoppingCart {
            background-color: #f5f5f5;
            padding-bottom: 20px;
        }

        /* 购物车头部CSS */
        .shoppingCart .cart-header {
            height: 64px;
            border-bottom: 2px solid #ff6700;
            background-color: #fff;
            margin-bottom: 20px;
        }

        .shoppingCart .cart-header .cart-header-content {
            width: 1225px;
            margin: 0 auto;
        }

        .shoppingCart .cart-header p {
            font-size: 28px;
            line-height: 58px;
            float: left;
            font-weight: normal;
            color: #424242;
        }

        .shoppingCart .cart-header span {
            color: #757575;
            font-size: 12px;
            float: left;
            height: 20px;
            line-height: 20px;
            margin-top: 30px;
            margin-left: 15px;
        }

        /* 购物车头部CSS END */

        /* 购物车主要内容区CSS */
        .shoppingCart .content {
            width: 1225px;
            margin: 0 auto;
            background-color: #fff;
        }

        .shoppingCart .content ul {
            background-color: #fff;
            color: #424242;
            line-height: 85px;
        }

        /* 购物车表头及CSS */
        .shoppingCart .content ul .header {
            height: 85px;
            padding-right: 26px;
            color: #424242;
        }

        .shoppingCart .content ul .product-list {
            height: 85px;
            padding: 15px 26px 15px 0;
            border-top: 1px solid #e0e0e0;
        }

        .shoppingCart .content ul .pro-check {
            float: left;
            height: 85px;
            width: 110px;
        }

        .shoppingCart .content ul .pro-check .el-checkbox {
            font-size: 16px;
            margin-left: 24px;
        }

        .shoppingCart .content ul .pro-img {
            float: left;
            height: 85px;
            width: 120px;
        }

        .shoppingCart .content ul .pro-img img {
            height: 80px;
            width: 80px;
        }

        .shoppingCart .content ul .pro-name {
            float: left;
            width: 380px;
        }

        .shoppingCart .content ul .pro-name a {
            color: #424242;
        }

        .shoppingCart .content ul .pro-name a:hover {
            color: #ff6700;
        }

        .shoppingCart .content ul .pro-price {
            float: left;
            width: 140px;
            padding-right: 18px;
            text-align: center;
        }

        .shoppingCart .content ul .pro-num {
            float: left;
            width: 150px;
            text-align: center;
        }

        .shoppingCart .content ul .pro-total {
            float: left;
            width: 120px;
            padding-right: 81px;
            text-align: right;
        }

        .shoppingCart .content ul .pro-total-in {
            color: #ff6700;
        }

        .shoppingCart .content ul .pro-action {
            float: left;
            width: 80px;
            text-align: center;
        }

        .shoppingCart .content ul .pro-action i:hover {
            color: #ff6700;
        }

        /* 购物车表头及CSS END */

        /* 购物车底部导航条CSS */
        .shoppingCart .cart-bar {
            width: 1225px;
            height: 50px;
            line-height: 50px;
            background-color: #fff;
        }

        .shoppingCart .cart-bar .cart-bar-left {
            float: left;
        }

        .shoppingCart .cart-bar .cart-bar-left a {
            line-height: 50px;
            margin-left: 32px;
            color: #757575;
        }

        .shoppingCart .cart-bar .cart-bar-left a:hover {
            color: #ff6700;
        }

        .shoppingCart .cart-bar .cart-bar-left .sep {
            color: #eee;
            margin: 0 20px;
        }

        .shoppingCart .cart-bar .cart-bar-left .cart-total {
            color: #757575;
        }

        .shoppingCart .cart-bar .cart-bar-left .cart-total-num {
            color: #ff6700;
        }

        .shoppingCart .cart-bar .cart-bar-right {
            float: right;
        }

        .shoppingCart .cart-bar .cart-bar-right .total-price-title {
            color: #ff6700;
            font-size: 14px;
        }

        .shoppingCart .cart-bar .cart-bar-right .total-price {
            color: #ff6700;
            font-size: 30px;
        }

        .shoppingCart .cart-bar .cart-bar-right .btn-primary {
            float: right;
            width: 200px;
            text-align: center;
            font-size: 18px;
            margin-left: 50px;
            background: #ff6700;
            color: #fff;
        }

        .shoppingCart .cart-bar .cart-bar-right .btn-primary-disabled {
            float: right;
            width: 200px;
            text-align: center;
            font-size: 18px;
            margin-left: 50px;
            background: #e0e0e0;
            color: #b0b0b0;
        }

        .shoppingCart .cart-bar .cart-bar-right .btn-primary:hover {
            background-color: #f25807;
        }

        /* 购物车底部导航条CSS END */
        /* 购物车主要内容区CSS END */

        /* 购物车为空的时候显示的内容CSS */
        .shoppingCart .cart-empty {
            width: 1225px;
            margin: 0 auto;
        }

        .shoppingCart .cart-empty .empty {
            height: 300px;
            padding: 0 0 130px 558px;
            margin: 65px 0 0;
            background: url(imgs/cart-empty.png) no-repeat 124px 0;
            color: #b0b0b0;
            overflow: hidden;
        }

        .shoppingCart .cart-empty .empty h2 {
            margin: 70px 0 15px;
            font-size: 36px;
        }

        .shoppingCart .cart-empty .empty p {
            margin: 0 0 20px;
            font-size: 20px;
        }

        .btn-login{
            width: 200px;
            background-color: #ff6700;
            float: left;
            height: 55px;
            font-size: 16px;
            color: #fff;
            border: none;
            text-align: center
        }
        .btn-login:hover{
            background-color: #f25807;
        }
        .gogogo{
            width: 200px;
            background-color: transparent;
            float: left;
            height: 55px;
            font-size: 16px;
            color: #ff6700;
            border: #ff6700 1px solid;
            text-align: center;
            margin-left: 10px;
        }

        /* 购物车为空的时候显示的内容CSS END */
    </style>
</head>
<body>
<%@ include file="header.jsp" %>
<!--头部-->
<div class="shoppingCart">
    <!-- 购物车头部 -->
    <div class="cart-header">
        <div class="cart-header-content">
            <p>
                <i class="el-icon-shopping-cart-full" style="color:#ff6700; font-weight: 600;"></i>
                我的购物车
            </p>
            <span>温馨提示：产品是否购买成功，以最终下单为准哦，请尽快结算</span>
        </div>
    </div>
    <!-- 购物车头部END -->


    <!-- 购物车主要内容区END -->

    <!-- 购物车为空的时候显示的内容 -->
    <div v-else class="cart-empty">
        <div class="empty">
            <h2>您的购物车还是空的！</h2>
            <p>快去购物吧！</p>
            <c:if test="${user==null}">
                <button class="btn-login"  onclick="javascript:window.open('/toLogin')" ><span>立即登录</span></button>
                <button class="gogogo"  onclick="javascript:location.href='/'"><span>马上去购物</span></button>
            </c:if>
            <c:if test="${user!=null}">
                <button class="btn-login" onclick="javascript:location.href='/'" ><span>马上去购物</span></button>
            </c:if>
        </div>



    </div>
    <!-- 购物车为空的时候显示的内容END -->
</div>
<%@ include file="footer.jsp" %>
</body>

</html>

