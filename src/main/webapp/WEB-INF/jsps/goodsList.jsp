<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>商品列表页</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <link rel="icon" href="img/小米手机/logo-footer.png" type="image/gif">
    <link rel="stylesheet" href="css/layui/css/layui.css">
    <link href="css/jyc.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="css/iconfont.css">

    <script src="css/layui/layui.js"></script>

    <script src="js/jquery-1.12.4.js"></script>
    <script src="js/jyc.js"></script>
    <link rel="stylesheet" href="css/index.css">
    <style type="text/css">
        .div1 {
            height: 260px;
            text-align: center;
        }
        .caption p{
            text-align: center;
            margin-top: 10px;
        }
        .thumbnail:hover{

           /* -webkit-box-shadow: #ccc 0px 5px 5px;

            -moz-box-shadow: #ccc 0px 5px 5px;

            box-shadow: #ccc 0px 5px 5px;*/
            box-shadow: 0 15px 30px rgba(0,0,0,0.1);
            transform: translate3d(0, -5px, 0);
        }
    </style>
    <script type="text/javascript" src="js/goods.js"></script>


</head>
<body>
<%@ include file="header.jsp" %>

<div class="panel panel-default" style="margin: 0 auto;width: 95%;">
    <div class="panel-heading">
        <h3 class="panel-title"><span class="glyphicon glyphicon-th-list"></span>&nbsp;&nbsp;商品列表</h3>
    </div>
    <div class="panel-body">
        <!--列表开始-->
        <div class="row ${fn:length(info.list)<=0?'div1':'' }">
            <c:if test="${fn:length(info.list)<=0 }">没有数据！！！</c:if>
            <div id="tbody">
                <c:forEach items="${info.list}" var="g" varStatus="i">
                    <a href="/toShow?id=${g.id}" target="_blank">
                    <div class="col-sm-3">
                        <div class="thumbnail">
                            <img src="img/picture/${g.picture}" style="width: 180px;height: 180px;" alt="${g.name}"/>
                            <div class="caption">
                                <p style="font-size: 16px">${g.name}</p>
                                <p>热销指数
                                    <c:forEach begin="1" end="${g.star}">
                                        <img src="image/star_red.gif" alt="star"/>
                                    </c:forEach>
                                </p>
                                <p>上架日期:<fmt:formatDate value="${g.pubdate}" pattern="yyyy-MM-dd"/> </p>
                                <p style="color:orange">价格:${g.price}</p>
                            </div>
                        </div>
                    </div>
                    </a>
                </c:forEach>
            </div>
        </div>


        <c:if test="${fn:length(info.list)>0 }">
            <nav aria-label="..." class="text-center">
                <ul class="pagination">
                    <li><a href="javascript:per()"><span>«</span></a></li>
                    <c:forEach begin="1" end="${info.pageCount}" var="num" step="1">
                        <li><a href="javascript:goto(${num})" name="title" <c:if test="${info.pageNo==num}">style="background-color: #bdbdbd" </c:if>>${num}</a></li>
                    </c:forEach>
                    <li><a href="javascript:next()"><span>»</span></a></li>
                </ul>
                <!--currPageNo 保存当前页码  -->
                <input type="hidden" value="${info.pageNo}" id="currPageNo"/>
                <!--totalPageCount 保存总页码  -->
                <input type="hidden" value="${info.pageCount}" id="totalPageCount"/>
                <input type="hidden" value="${typeId}" id="typeId"/>
                <input type="hidden" value="${name}" id="name"/>

            </nav>
        </c:if>

    </div>
</div>
<!-- 底部 -->
<%@ include file="footer.jsp" %>


</body>
</html>