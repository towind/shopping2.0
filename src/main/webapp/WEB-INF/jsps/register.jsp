<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>注册</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <link rel="SHORTCUT ICON" href="img/Router/mi.png">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/login.css">
    <script type="text/javascript" src="js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/messages_zh.js"></script>
    <script type="text/javascript">
        $(function () {
            var flag = true;
            $("#username").blur(function () {
                var username = $("#username").val();
                $.post("/cation", {username: username}, function (result) {
                    if (result) {
                        $("#usernameMsg").html("用户名已经存在").css("color", "red");
                        $("#registerBtn").prop("disabled", true);
                        flag = false;
                    } else {
                        if (username!="") {
                            $("#usernameMsg").html("用户名可用").css("color", "green");
                            $("#registerBtn").removeAttr("disabled");
                            flag = true;
                        }else{
                            $("#usernameMsg").html("")
                            $("#registerBtn").prop("disabled", true);
                            flag = false;
                        }
                    }
                }, "JSON");
            });



            $("#registerBtn").click(function () {
                $("#error1").hide()
                $("#error2").hide()
                $("#error3").hide()
                $("#error4").hide()
                if (flag) {
                    var username = $("#username").val();
                    var password = $("#password").val();
                    var repassword = $("#repassword").val();
                    var email = $("[name=email]").val();
                    var gender = $("[name=gender]:checked").val();
                    var reg=/^[a-zA-Z0-9]{1,}@[a-zA-Z0-9]{1,}.com$/

                    if (password.length<6) {
                        return;
                    }
                    if (password == "") {
                        $("#error2").show()
                        return;
                    }
                    if (repassword == "") {
                        $("#error3").show()
                        return;
                    }
                    if (email == "") {
                        $("#error4").show()
                        return;
                    }
                    if (password!=repassword) {
                        return;
                    }
                    if (!reg.test(email)) {
                        return;
                    }
                    $.post("/toAdd", {
                        username: username,
                        password: password,
                        email: email,
                        gender: gender
                    }, function (result) {
                        if (result) {
                            location.href = "/regSuc";
                        } else {
                            alert("注册失败！");
                        }
                    }, "JSON")
                }


            });

            $("#form1").validate({
                rules: {
                    username: "required",
                    password: {
                        required: true,
                        minlength: 6,
                        maxlength: 16
                    },
                    repassword: {
                        equalTo: "#password"
                    },
                    email: "email"
                },
                messages: {
                    username: "用户名不能为空"
                }
            });

        });
    </script>


</head>

<body>
<div class="regist">
    <div class="regist_center">
        <div class="regist_top">
            <div class="left fl"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;会员注册</div>
            <div class="right fr">
                <a href="index.jsp" target="_black">小米商城</a>
            </div>
            <div class="clear"></div>
            <div class="xian center"></div>
        </div>
        <div class="center-block" style="margin-top: 80px;">
            <form id="form1" class="form-horizontal" action="javascript:void(0)" method="post">

                <div class="form-group">
                    <label class="col-sm-2 control-label">用户名</label>
                    <div class="col-sm-8" style="width: 40%">
                        <input type="text" id="username" name="username" class="form-control col-sm-10"
                               placeholder="Username"/>
                    </div>
                    <div class="col-sm-2">
                        <p class="text-danger"><span id="usernameMsg" class="help-block "></span>
                            <lable class="error" for="username"></lable>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">密码</label>
                    <div class="col-sm-8" style="width: 40%">
                        <input type="password" name="password" id="password"
                               class="form-control col-sm-10" placeholder="Password"/>
                        <label id="error2" class='error' style="display: none">密码不能为空</label>
                    </div>
                    <div class="col-sm-2">
                        <p class="text-danger"><span id="passwordMsg" class="help-block ">请输入6位以上字符</span></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">确认密码</label>
                    <div class="col-sm-8" style="width: 40%">
                        <input type="password" name="repassword" id="repassword" class="form-control col-sm-10"
                               placeholder="Password Again"/>
                        <label id="error3" class='error' style="display: none">第二次密码不能为空</label>
                    </div>
                    <div class="col-sm-2">
                        <p class="text-danger"><span id="repasswordMsg" class="help-block ">两次密码要输入一致哦</span></p>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">邮箱</label>
                    <div class="col-sm-8" style="width: 40%">
                        <input type="email" name="email" class="form-control col-sm-10"
                               placeholder="Email"/>
                        <label id="error4" class='error' style="display: none">邮箱不能为空</label>
                    </div>
                    <div class="col-sm-2">
                        <p class="text-danger"><span id="emailMsg" class="help-block ">填写正确邮箱格式</span></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">性别</label>
                    <div class="col-sm-8" style="width: 40%">
                        <label class="radio-inline"> <input type="radio"
                                                            name="gender" value="男" checked="checked"> 男
                        </label> <label class="radio-inline"> <input type="radio"
                                                                     name="gender" value="女"> 女
                    </label>
                    </div>
                    <div class="col-sm-2">
                        <p class="text-danger"><span id="genderMsg" class="help-block ">你是帅哥 还是美女</span></p>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <div class="col-sm-7 col-sm-push-2">
                        <input id="registerBtn" type="button" value="注册" class="btn btn-primary  btn-lg"
                               style="width: 200px;"/> &nbsp; &nbsp;
                        <input type="reset" value="重置" class="btn btn-default  btn-lg" style="width: 200px;"/>
                    </div>
                </div>
                <div class="text-center" style="color:red">${registerMsg}</div>
            </form>

        </div>
    </div>
</div>

</body>
</html>
