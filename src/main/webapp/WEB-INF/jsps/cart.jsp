<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="SHORTCUT ICON" href="img/Router/mi.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>购物车</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.12.4.js"></script>
    <link rel="icon" href="img/小米手机/logo-footer.png" type="image/gif" >
    <link rel="stylesheet" href="css/layui/css/layui.css">
    <link href="css/jyc.css" rel="stylesheet"  type="text/css">
    <link rel="stylesheet" type="text/css" href="css/iconfont.css">


    <script src="css/layui/layui.js"></script>

    <script src="js/jquery-3.3.1.js"></script>
    <script src="js/jyc.js"></script>
    <link rel="stylesheet" href="css/index.css">
    <script type="text/javascript">



        $(function(){
            var inp=1;
            xiao();
            $("[name=add]").click(function(){

                var adds= $(this).parent().prev().val();
                //adds获取到商品当前个数
                adds++;
                inp=adds;
                $(this).parent().prev().val(adds);
                var money=$(this).prev().val();
                var sum=parseFloat(money)*parseFloat(adds);
                $(this).parent().parent().parent().next().html("¥&nbsp;"+sum);

                var id=$("[name=id]").val();
                var pid=$(this).prev().prev().val();

                $.post("/updNum",{num:adds,money:sum,id:id,pid:pid},function(data){
                    if(data.code==0){

                        xiao();
                    }else{
                        alert("更新失败!");
                    }


                },"JSON");
            });

            $("[name=del]").click(function(){

                if($(this).parent().next().val()!=1){

                    var adds= $(this).parent().next().val();
                    adds--;
                    $(this).parent().next().val(adds);

                    var price=$(this).prev().prev().prev().val();//单价
                    var money=$(this).parent().parent().parent().next().html();

                    var moneys=money.substring(7);
                    var sum=parseFloat(moneys)-parseFloat(price);
                    $(this).parent().parent().parent().next().html("¥&nbsp;"+sum);

                    var id=$("[name=id]").val();
                    var pid=$(this).prev().val();

                    $.post("/updNum",{num:adds,money:sum,id:id,pid:pid},function(data){
                        if(data.code==0){

                            xiao();
                        }else{
                            alert("更新失败!");
                        }


                    },"JSON");
                }else{
                    alert("至少买一个吧!");
                }
            });
        });

        function xiao() {
            var money=$("[name=money]");
            var sums=0;
            money.each(function() {
                var moneys=parseFloat($(this).html().substring(7));
                sums+=moneys;
            });
            $("#b").html("￥&nbsp;&nbsp;"+sums);
        }

        function deleteCart(el,pid){
            if(confirm("确定要移除该商品吗?")){
                $.post("/delCart",{pid:pid},function(result){
                    if(result.code==0){

                        alert("移除成功!");
                        if($("tr").length==1){
                            $("#removeAllProduct").css("display","none");
                        }else{
                            $("#removeAllProduct").css("display","");
                        }
                        xiao();
                    }else{
                        alert("移除失败");
                    }
                },"JSON")
            }
        }
        function delCard(pid) {
            if(confirm("确定要移除该商品吗?")){
                location.href="/delCart?pid="+pid;
            }
        }


        function deleteCartAll(id){
            if(confirm("确定要清空购物车?")){
                location.href="/delAllCart?id="+id;
            }
        }






    </script>


</head>
<body style="background-color:#f5f5f5">
<c:if test="${user==null }">
    <c:redirect url="login.jsp"></c:redirect>
</c:if>
<%@ include file="header.jsp" %>

<div style="height: 10px"> </div>
<div class="container" style="background-color: white;">
    <div class="row" style="margin-left: 40px">
        <h3>我的购物车
            <small>温馨提示：产品是否购买成功，以最终下单为准哦，请尽快结算</small>
        </h3>
    </div>
    <div class="row" style="margin-top: 40px;">
        <div class="col-md-10 col-md-offset-1">
            <table class="table table-bordered table-striped table-hover">
                <tr>
                    <th>序号</th>
                    <th>商品名称</th>
                    <th>价格</th>
                    <th>数量</th>
                    <th>小计</th>
                    <th>操作</th>
                </tr>
                <c:set value="0" var="sum"></c:set>
                <c:forEach items="${cartList}" var="c" varStatus="i">
                    <tr name="xunhuan">
                        <th>${i.count}</th>
                        <th>${c.goods.name}</th>
                        <th>${c.goods.price}</th>
                        <th width="100px">
                            <div class="input-group">
		 						<span class="input-group-btn">
		 						<input type="hidden" value="${c.goods.price}"/>
		 						<input type="hidden" name="id" value="${c.id}"/>
	 							<input type="hidden" name="pid" value="${c.pid}"/>
		 							<button class="btn btn-default" name="del" id="del" type="button">-</button>
		 						</span>
                                <input type="text" class="form-control" name="num_count" id="num_count${i.count}"
                                       value="${c.num}" readonly="readonly" style="width:40px">
                                <span class="input-group-btn">
		 						
	 							<input type="hidden" name="id" value="${c.id}"/>
	 							<input type="hidden" name="pid" value="${c.pid}"/>
		 						<input type="hidden" value="${c.goods.price}"/>
		 						
		 						
		 							<button class="btn btn-default" name="add" type="button">+</button>
		 						</span>
                            </div>
                        </th>
                        <th name="money">¥&nbsp;${c.money}</th>
                        <th>


                            <button type="button" class="btn btn-default" onclick="delCard(${c.pid})">删除
                            </button>
                        </th>
                    </tr>
                    <c:set var="sum" value="${sum+c.money}"></c:set>
                </c:forEach>
            </table>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="pull-right" style="margin-right: 40px;">

            <div>
                <c:choose>
                    <c:when test="${fn:length(cartList)==0}">
                        <a id="removeAllProduct" href="javascript:clearCart()" class="btn btn-default btn-lg"
                           onclick="deleteCartAll(${user.id})" style="display:none">清空购物车</a>
                        &nbsp;&nbsp;
                    </c:when>
                    <c:otherwise>
                        <a id="removeAllProduct" href="javascript:clearCart()" class="btn btn-default btn-lg"
                           onclick="deleteCartAll(${user.id})">清空购物车</a>
                        &nbsp;&nbsp;
                    </c:otherwise>
                </c:choose>

                <c:if test="${not empty user }">
                    <c:choose>
                        <c:when test="${fn:length(cartList)==0}">
                            <a href="order.jsp" class="btn  btn-danger btn-lg">结账</a>
                        </c:when>
                        <c:otherwise>
                            <a href="/toOrder" class="btn  btn-danger btn-lg" id="clear">结账</a>
                        </c:otherwise>
                    </c:choose>
                </c:if>

            </div>
            <br><br>
            <div style="margin-bottom: 20px;">
                商品金额总计：<span id="total" class="text-danger"><b id="b"></b></span>
            </div>
        </div>
    </div>
</div>

<div style="height: 40px"> </div>

<!-- 底部 -->
<%@ include file="footer.jsp" %>


</body>
</html>