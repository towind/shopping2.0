<%--
  Created by IntelliJ IDEA.
  User: Gosick
  Date: 2022/4/9
  Time: 20:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <link rel="icon" href="img/小米手机/logo-footer.png" type="image/gif" >
    <link rel="stylesheet" href="css/layui/css/layui.css">
    <link href="css/jyc.css" rel="stylesheet"  type="text/css">
    <link rel="stylesheet" type="text/css" href="css/iconfont.css">

    <script src="css/layui/layui.js"></script>

    <script src="js/jquery-1.12.4.js"></script>
    <script src="js/jyc.js"></script>
    <link rel="stylesheet" href="css/index.css">

    <style>
        /* 头部CSS */
        #details .page-header {
            height: 64px;
            margin-top: -20px;
            z-index: 4;
            background: #fff;
            border-bottom: 1px solid #e0e0e0;
            -webkit-box-shadow: 0px 5px 5px rgba(0, 0, 0, 0.07);
            box-shadow: 0px 5px 5px rgba(0, 0, 0, 0.07);
        }
        #details .page-header .title {
            width: 1225px;
            height: 64px;
            line-height: 64px;
            font-size: 18px;
            font-weight: 400;
            color: #212121;
            margin: 0 auto;
        }
        #details .page-header .title p {
            float: left;
        }
        #details .page-header .title .list {
            height: 64px;
            float: right;
        }
        #details .page-header .title .list li {
            float: left;
            margin-left: 20px;
        }
        #details .page-header .title .list li a {
            font-size: 14px;
            color: #616161;
        }
        #details .page-header .title .list li a:hover {
            font-size: 14px;
            color: #ff6700;
        }
        /* 头部CSS END */

        /* 主要内容CSS */
        #details .main {
            width: 1225px;
            height: 560px;
            padding-top: 30px;
            margin: 0 auto;
        }
        #details .main .block {
            float: left;
            width: 560px;
            height: 560px;
        }
        #details .el-carousel .el-carousel__indicator .el-carousel__button {
            background-color: rgba(163, 163, 163, 0.8);
        }
        #details .main .content {
            float: left;
            margin-left: 25px;
            width: 640px;
        }
        #details .main .content .name {
            height: 30px;
            line-height: 30px;
            font-size: 24px;
            font-weight: normal;
            color: #212121;
        }
        #details .main .content .intro {
            color: #b0b0b0;
            padding-top: 10px;
        }
        #details .main .content .store {
            color: #ff6700;
            padding-top: 10px;
        }
        #details .main .content .price {
            display: block;
            font-size: 18px;
            color: #ff6700;
            border-bottom: 1px solid #e0e0e0;
            padding: 25px 0 25px;
        }
        #details .main .content .price .del {
            font-size: 14px;
            margin-left: 10px;
            color: #b0b0b0;
            text-decoration: line-through;
        }
        #details .main .content .pro-list {
            background: #f9f9fa;
            padding: 30px 60px;
            margin: 50px 0 50px;
        }
        #details .main .content .pro-list span {
            line-height: 30px;
            color: #616161;
        }
        #details .main .content .pro-list .pro-price {
            float: right;
        }
        #details .main .content .pro-list .pro-price .pro-del {
            margin-left: 10px;
            text-decoration: line-through;
        }
        #details .main .content .pro-list .price-sum {
            color: #ff6700;
            font-size: 24px;
            padding-top: 20px;
        }
        #details .main .content .button {
            height: 55px;
            margin: 10px 0 20px 0;
        }
        #details .main .content .button .el-button {
            float: left;
            height: 55px;
            font-size: 16px;
            color: #fff;
            border: none;
            text-align: center;
        }
        #details .main .content .button .shop-cart {
            width: 340px;
            background-color: #ff6700;
        }
        #details .main .content .button .shop-cart:hover {
            background-color: #f25807;
        }

        #details .main .content .button .like {
            width: 260px;
            margin-left: 40px;
            background-color: #b0b0b0;
        }
        #details .main .content .button .like:hover {
            background-color: #757575;
        }
        #details .main .content .pro-policy li {
            float: left;
            margin-right: 20px;
            color: #b0b0b0;
        }
        /* 主要内容CSS END */


        .layui-carousel[lay-arrow=always] .layui-carousel-arrow{
            left: 20px;
            top: 40%;
        }

        .layui-carousel-ind li{
            width: 20px;
            height: 2px;
            border-radius:5%;
        }

        .layui-carousel-ind{
            top: -55px;
            width:460px;
        }
    </style>
    <script src="css/layui/layui.js"></script>

    <script type="text/javascript">
        $(function () {
            layui.use('carousel', function(){
                var carousel = layui.carousel;
                //建造实例
                carousel.render({
                    elem: '#home-carousel'
                    ,width: '560px' //设置容器宽度
                    ,height: '560px'
                    ,arrow: 'always' //始终显示箭头
                    ,number:'3000'
                    //,anim: 'updown' //切换动画方式

                });
            });

            $.post("/getPicture",null,function (result) {
                var str="";
                $(result.data).each(function () {
                    str+="<div>"
                    str+="<img src='img/picture/"+this.picture+"'/>"
                    str+="</div>"
                })
                $("#imgadd").html(str)

            },"JSON")
        })

        function addCart(id,price) {
            $.post("/toAddCart",{id:id,money:price},function (result) {
                if (result.data==1){
                    alert("添加购物车成功")
                } else if(result.data==-1){
                   window.open("/toLogin")
                }else{
                    alert("添加购物车失败")
                }

            },"JSON")
        }
    </script>

</head>
<body>
<%@ include file="header.jsp" %>

<div style="height: 30px"></div>

<div id="details">
    <!-- 头部 -->
    <div class="page-header">
        <div class="title">
            <p style="float: left">${goods.name}</p>
            <div class="list">
                <ul>
                    <li>
                        <router-link to>概述</router-link>
                    </li>
                    <li>
                        <router-link to>参数</router-link>
                    </li>
                    <li>
                        <router-link to>用户评价</router-link>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- 头部END -->

    <!-- 主要内容 -->
    <div class="main">
        <!-- 左侧商品轮播图 -->
        <div class="block">
            <div class="home-carousel">
                <div class="layui-carousel" id="home-carousel" lay-filter="test1">
                    <div carousel-item id="imgadd">
                        <%--<c:forEach items="" var="goods">
                            <div>
                                <img src="img/index/home-carousel1.jpg"/>
                            </div>
                        </c:forEach>--%>
                    </div>
                </div>
            </div>
        </div>
        <!-- 左侧商品轮播图END -->

        <!-- 右侧内容区 -->
        <div class="content">
            <h1 class="name">${goods.name}</h1>
            <p class="intro">${goods.intro}</p>
            <p class="store">小米自营</p>
            <div class="price">
                <span>${goods.price}元</span>
                <span class="del"><c:if test="${goods.oldPrice!=0}">${goods.oldPrice}元</c:if></span>
            </div>
            <div class="pro-list">
                <span class="pro-name">${goods.name}</span>
                <span class="pro-price">
                    <span>${goods.price}元</span>
                    <span class="pro-del"><c:if test="${goods.oldPrice!=0}">${goods.oldPrice}元</c:if></span>
                </span>
                <p class="price-sum">总计 : ${goods.price}元</p>
            </div>
            <!-- 内容区底部按钮 -->
            <div class="button">
                <button class="el-button shop-cart el-button--default" onclick="addCart(${goods.id},${goods.price})"><span>加入购物车</span></button>
                <button class="like el-button shop-cart el-button--default" ><span>喜欢</span></button>
            </div>
            <!-- 内容区底部按钮END -->
            <div class="pro-policy">
                <ul>
                    <li>
                        <i class="el-icon-circle-check"></i> 小米自营
                    </li>
                    <li>
                        <i class="el-icon-circle-check"></i> 小米发货
                    </li>
                    <li>
                        <i class="el-icon-circle-check"></i> 7天无理由退货
                    </li>
                    <li>
                        <i class="el-icon-circle-check"></i> 7天价格保护
                    </li>
                </ul>
            </div>
        </div>
        <!-- 右侧内容区END -->
    </div>
    <!-- 主要内容END -->
</div>

<%@ include file="footer.jsp" %>
</body>
</html>
