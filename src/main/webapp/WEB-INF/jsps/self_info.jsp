<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>个人中心-收货地址页面</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <link rel="stylesheet" type="text/css" href="css/login2.css">
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="icon" href="img/小米手机/logo-footer.png" type="image/gif">
    <link rel="stylesheet" href="css/layui/css/layui.css">
    <link href="css/jyc.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="css/iconfont.css">
    <link rel="stylesheet" type="text/css" href="fonts/iconfont.css">

    <script src="css/layui/layui.js"></script>

    <script src="js/jquery-3.3.1.js"></script>
    <link rel="stylesheet" href="css/index.css">
    <script src="js/jyc.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
        function Sane() {
            var name = $("#name").val();
            var phone = $("#phone").val();
            var detail = $("#detail").val();
            var uid = $("#uid").val();
            $.post("/addAddress", {name: name, phone: phone, detail: detail, uid: uid}, function (result) {
                if (result.code == "0") {
                    initD();
                }
                alert(result.msg)

            }, "JSON");

        }

        function updAddress(id) {
            var updUid = $("#updUid").val();
            var updName = $("#updName").val();
            var updPhone = $("#updPhone").val();
            var updDetail = $("#updDetail").val();
            $.post("/updAddress", {
                id: id,
                updUid: updUid,
                updName: updName,
                updPhone: updPhone,
                updDetail: updDetail
            }, function (result) {
                if (result.code == "0") {
                    location.href="/Address"
                    /*$(".modal-backdrop").remove()
                    initD()*/
                }
                alert(result.msg)
            }, "JSON");
        }


        function deleteAddress(id) {
            var res = confirm("是否删除");
            if (res == true) {
                $.post("/deleteAddress", {id: id}, function (result) {
                    if (result.code == "0") {
                        initD()
                    }
                    alert(result.msg)

                }, "JSON")
            }
        }

        function defaultAddr(id) {
            var res = confirm("是否设为默认");
            if (res == true) {
                $.post("/defaultAddr", {id: id}, function (result) {
                    if (result.code == "0") {
                        initD()
                    }
                    alert(result.msg)

                }, "JSON")

            }
        }

        function initD() {
            $.post("/getAddressList", null, function (result) {
                var str = "";
                var i = 0;
                $(result.data).each(function () {
                    i=i+1;
                    str += "<tr>";
                    str += "<td>"+i+"</td>";
                    str += "<td>" + this.name + "</td>";
                    str += "<td>" + this.phone + "</td>";
                    str += "<td>" + this.detail + "</td>";
                    str += "<td>";
                    str += "<button onclick='deleteAddress(" + this.id + ")' class='btn btn-danger btn-sm'>删除</button>&nbsp;&nbsp;";
                    str += "<button class='btn btn-default btn-sm' data-toggle='modal' data-target='#myModal" + this.id + "'>修改</button>&nbsp;&nbsp;";
                    str += "<!-- 弹出模态框 -->";

                    str += "<div class='modal fade' tabindex='-1' role='dialog' id='myModal" + this.id + "'>";
                    str += "<div class='modal-dialog' role='document'>";
                    str += "<div class='modal-content'>";
                    str += "<div class='modal-header'>";
                    str += "<button type='button'  style='float: right;height: 30px;width: 30px' data-dismiss='modal'>";
                    str += "<span >&times;</span>";
                    str += "</button>";
                    str += "<h4 class='modal-title'>修改地址</h4>";
                    str += "</div>";
                    str += "<form  method='post' class='form-horizontal'>";
                    str += "<div class='motal-body'>";
                    str += "<div class='form-group'>";
                    str += "<label class='col-sm-2 control-label'>收件人</label>";
                    str += "<div class='col-sm-10'>";
                    str += "<input type='hidden' name='id' value='" + this.id + "'>";
                    str += "<input type='hidden' name='level' value='" + this.level + "'>";
                    str += "<input type='text' name='updName' class='form-control' value='" + this.name + "' id='updName'>";
                    str += "</div>";
                    str += "</div>";
                    str += "<div class='form-group'>";
                    str += "<label class='col-sm-2 control-label'>电话</label>";
                    str += "<div class='col-sm-10'>";
                    str += "<input type='text' name='updPhone' class='form-control' value='" + this.phone + "' id='updPhone'>";
                    str += "</div>";
                    str += "</div>";
                    str += "<div class='form-group'>";
                    str += "<label class='col-sm-2 control-label'>详细地址</label>";
                    str += "<div class='col-sm-10'>";
                    str += "<input type='text' name='updDetail' class='form-control' value='" + this.detail + "'  id='updDetail'>";
                    str += "</div>";
                    str += "</div>";

                    str += "</div>";
                    str += "<div class='motal-footer text-center' style='padding-bottom: 20px'>";
                    str += "<button type='button' class='btn btn-primary'  onclick='updAddress(" + this.id + ")'>修改</button>";
                    str += "</div>";
                    str += "</form>";
                    str += "</div>";
                    str += "</div>";
                    str += "</div>";

                    str += "<button onclick='defaultAddr(" + this.id + ")' class='btn btn-primary btn-sm'>设为默认</button>";
                    if (this.level == 1) {
                        str += "<span class='badge' style='background-color:red;'>默认</span>"
                    } else {
                        str += "<span class='badge'>普通</span>"
                    }
                    str += "</td>";
                    str += "</tr>"
                })
                $("#tbody").html(str);
            }, "JSON")
        }


    </script>


</head>

<body>
<%@ include file="header.jsp" %>
<!--网站中间内容开始-->
<div id="dingdanxiangqing_body">
    <div id="dingdanxiangqing_body_big">
        <div id="big_left">
            <p style="font-size:18px;margin-top: 15px">订单中心</p>
            <a id="big_left_a" href="OrderDetailViewServlet?opr=getOrderList">我的订单</a><br/>
            <a id="big_left_a" href="">意外保</a><br/>
            <a id="big_left_a" href="">团购订单</a><br/>
            <a id="big_left_a" href="">评价晒单</a><br/>
            <p style="font-size:18px">个人中心</p>
            <a id="big_left_a" href="self_info.html">我的个人中心</a><br/>
            <a id="big_left_a" href="">消息通知</a><br/>
            <a id="big_left_a" href="">优惠券</a><br/>
            <a id="big_left_a" href="">收货地址</a><br/>
        </div>
        <div id="big_right" style="height: 500px;overflow: scroll;">

            <div style="margin:0 20px;">
                <h3>收货地址</h3>
                <hr>
                <table class="table table-striped table-hover table-bordered">
                    <tr>
                        <th>序号</th>
                        <th>收件人</th>
                        <th>手机号</th>
                        <th>地址</th>
                        <th>操作</th>
                    </tr>
                    <tbody id="tbody">
                    <c:forEach var="address" items="${addressList}" varStatus="i">
                        <tr>
                            <Td>${i.count}</Td>
                            <td>${address.name}</td>
                            <td>${address.phone}</td>
                            <td>${address.detail}</td>
                            <td>
                                <button onclick="deleteAddress(${address.id})" class="btn btn-danger btn-sm">删除</button>&nbsp;&nbsp;
                                <button class="btn btn-default btn-sm" data-toggle="modal"
                                        data-target="#myModal${address.id}">修改
                                </button>&nbsp;&nbsp;
                                <!-- 弹出模态框 -->

                                <div class="modal fade" tabindex="-1" role="dialog" id="myModal${address.id}">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" style="float: right;height: 30px;width: 30px"
                                                        data-dismiss="modal">
                                                    <span>&times;</span>
                                                </button>
                                                <h4 class="modal-title">修改地址</h4>
                                            </div>
                                            <form method="post" class="form-horizontal">
                                                <div class="motal-body">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">收件人</label>
                                                        <div class="col-sm-10">
                                                            <input type="hidden" name="id" value="${address.id}">
                                                            <input type="hidden" name="level" value="${address.level}">

                                                            <input type="text" name="updName" class="form-control"
                                                                   value="${address.name}" id="updName">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">电话</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" name="updPhone" class="form-control"
                                                                   value="${address.phone}" id="updPhone">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">详细地址</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" name="updDetail" class="form-control"
                                                                   value="${address.detail}" id="updDetail">
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="motal-footer text-center" style="padding-bottom: 20px">
                                                    <button type="button" class="btn btn-primary"
                                                            onclick="updAddress(${address.id})">修改
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <button onclick="defaultAddr(${address.id})"
                                        class="btn btn-primary btn-sm">设为默认
                                </button>
                                <c:if test="${address.level==1}">
                                    <span class="badge" style="background-color:red;">默认</span>
                                </c:if>
                                <c:if test="${address.level==0}">
                                    <span class="badge">普通</span>
                                </c:if>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <br>
            <div class="container" style="width:960px;">

                <form method="post" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 form-label">收件人</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="name" id="name"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 form-label">手机号</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="phone" id="phone"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">详细地址</label>
                        <textarea rows="3" class="form-control" name="detail" id="detail"></textarea>
                    </div>
                    <div class="form-group col-md-12">
                        <input type="button" class="btn btn-primary" onclick="Sane()" value="添加地址">
                    </div>
                    <input type="hidden" value="${user.id}" name="uid" id="uid">
                </form>
            </div>
        </div>
    </div>
</div>
<input type="hidden" value="${user.id}" name="uid"
       id="updUid">
<!-- 底部 -->
<%@ include file="footer.jsp" %>

</body>
</html>
