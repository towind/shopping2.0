<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!--网站版权部分开始-->
<div class="clear"></div>
<div class="footer">
    <div class="wrap">
        <div class="footer-service">
            <ul>
                <li><a><i class="iconfont">&#xe715;</i>预约维修服务</a></li>
                <li><a><i class="iconfont">&#xe67d;</i>7天无理由退货</a></li>
                <li><a><i class="iconfont">&#xe639;</i>15天免费换货</a></li>
                <li><a><i class="iconfont">&#xe608;</i>满150元包邮</a></li>
                <li><a><i class="iconfont">&#xe613;</i>520余家售后网点</a></li>
            </ul>
        </div>
        <div class="footer-links">
            <dl>
                <dt>帮助中心</dt>
                <dd>账户管理</dd>
                <dd>购物指南</dd>
                <dd>订单操作</dd>
            </dl>
            <dl>
                <dt>服务支持</dt>
                <dd>售后政策</dd>
                <dd>自助服务</dd>
                <dd>相关下载</dd>
            </dl>
            <dl>
                <dt>线下门店</dt>
                <dd>小米之家</dd>
                <dd>服务网点</dd>
                <dd>零售网点</dd>
            </dl>
            <dl>
                <dt>关于小米</dt>
                <dd>了解小米</dd>
                <dd>加入小米</dd>
                <dd>联系我们</dd>
            </dl>
            <dl>
                <dt>关注我们</dt>
                <dd>新浪微博</dd>
                <dd>小米部落</dd>
                <dd>官方微信</dd>
            </dl>
            <dl>
                <dt>特色服务</dt>
                <dd>F 码通道</dd>
                <dd>礼物码</dd>
                <dd>防伪查询</dd>
            </dl>
            <div class="footer-contact">
                <p class="phone">400-100-5678</p>
                <p >周一至周日 8:00-18:00<br>（仅收市话费）</p>
                <a class="footer-btn"> 在线客服</a>
            </div>
        </div>
    </div>
</div>
<div class="clear"></div>
<div class="site-info">
    <div class="wrap">
        <div class="logo"><img src="img/小米手机/logo-footer.png"/></div>
        <div class="site-text">
            <p class="sites">
                <a>小米商城</a><span>|</span>
                <a>MIUI</a><span>|</span>
                <a>米家</a><span>|</span>
                <a>米聊</a><span>|</span>
                <a>多看</a><span>|</span>
                <a>路由器</a><span>|</span>
                <a>米粉卡</a><span>|</span>
                <a>小米天猫店</a><span>|</span>
                <a>隐私政策</a><span>|</span>
                <a>问题反馈</a><span>|</span>
                <a>Select Region</a><span>|</span>
            </p>
            <p>@<a>mi.com</a>" 京ICP证110507号 "<a>京ICP备10046444号</a><a>京公网安备11010802020134号 </a>
                <a>京网文[2014]0059-0009号</a>
                <br>
                " 违法和不良信息举报电话：185-0130-1238，本网站所列数据，除特殊说明，所有数据均出自我司实验室测试"
            </p>
        </div>
        <div class="site-links">
            <a><img src="img/小米手机/truste.png"/></a>
            <a><img src="img/小米手机/v-logo-2.png"/></a>
            <a><img src="img/小米手机/v-logo-1.png"/></a>
            <a><img src="img/小米手机/v-logo-3.png"/></a>
            <a><img src="img/小米手机/v-logo-4.png" height="28" width="85"/></a>
        </div>
        <div class="clear"></div>
        <div class="slogan"></div>
    </div>

</div>
</div>