<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<script type="text/javascript">
    $(function () {
        $.post("/getGoodType", null, function (data) {
            $(data.data).each(function () {
                if (this.level==1){
                    var a = "<li class='nav-item'><a href='/toSearch?typeId="+this.id+"'>"+this.name+"</a></li>";
                    $("#nav-list").append(a);
                }
            })
        },"JSON")

        $("#myform").submit(function () {
            var name=$("#goodsName").val()
            $("#name").val(name)
            if(name==""){
                return false;
            }
            return true;
        })
    })
</script>




<div class="header">
    <div class="topbar">
        <div class="wrap">
            <div class="topbar-left">
                <a>小米商城</a><span>|</span>
                <a>MIUI</a><span>|</span>
                <a>loT</a><span>|</span>
                <a>云服务</a><span>|</span>
                <a>水滴</a><span>|</span>
                <a>金融</a><span>|</span>
                <a>有品</a><span>|</span>
                <a>Select Region</a>
            </div>
            <div class="topbar-right">


           		<c:if test="${empty user}">
                    <a href="/toLogin" id="login">登录</a><span style="margin:0 .5em;color:#424242;">|</span>
                    <a href="/toUser" id="register">注册</a><span style="margin:0 .5em;color:#424242;">|</span>
                </c:if>
       			<c:if test="${not empty user}">
                    <a href="AddressServlet?opr=getAddress&id=${user.id}">${user.username}</a>
                    <span style="margin:0 .5em;color:#424242;">|</span>
                    <a href="/toExit" onclick="return confirm('确定要退出吗?')">注销</a>
                    <span style="margin:0 .5em;color:#424242;">|</span>
                    <a href="/getOrder">我的订单</a>
                    <span style="margin:0 .5em;color:#424242;">|</span>
                    <a href="/ToAddress?id=${user.id}">地址管理</a>
                </c:if>

                <div id="topbar-cart">
                    <c:choose>
                        <c:when test="${user!=null}">
                            <a href="goCart?id=${user.id}">
                                <i class="iconfont">&#xe600;</i>购物车
                            </a>
                        </c:when>
                        <c:otherwise>
                            <a href="/toCart">
                                <i class="iconfont">&#xe600;</i>购物车
                            </a>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="loading" id="load"  style="display:none;">购物车中还没有商品，赶紧选购吧！</div>
            </div>
        </div>
    </div>
    <div class="nav wrap">
        <div class="nav-logo">
            <a href="/"><img src="img/小米手机/logo-footer.png"></a>
        </div>
        <div class="header-nav">
            <ul class="nav-list" id="nav-list">
                <%--<li class="nav-item" id="phone">
                    <a>小米手机</a>
                    <div class="nav-bar-down" id="navbar1">
                        <div class="wrap">
                            <div>
                                <img class="firstimg" src="img/小米手机/mix2320-220.png"/>
                            </div>
                            <div>
                                <img src="img/小米手机/note2320x220.png"/>
                            </div>
                            <div>
                                <img src="img/小米手机/xm6-320.png"/>
                            </div>
                            <div>
                                <img src="img/小米手机/max2_toubu.png"/>
                            </div>
                            <div>
                                <img src="img/小米手机/5x-2!160x110.jpg"/>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="nav-item" id="hongmi">
                    <a>红米</a>
                    <div class="nav-bar-down" id="navbar2">
                        <div class="wrap">
                            <div>
                                <img class="firstimg" src="img/小米手机/5-320-220.png"/>
                            </div>
                            <div>
                                <img src="img/小米手机/5P-320-220.png"/>
                            </div>
                            <div>
                                <img src="img/小米手机/320-220-1.png"/>
                            </div>
                            <div>
                                <img src="img/小米手机/3205a.png"/>
                            </div>
                            <div>
                                <img src="img/小米手机/320-220!160x110.jpg"/>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="nav-item" id="tv">
                    <a>电视</a>

                    <div class="nav-bar-down" id="navbar3">
                        <div class="wrap">
                            <div>
                                <img class="firstimg" src="img/小米手机/tv1.png"/>
                            </div>
                            <div>
                                <img src="img/小米手机/tv2.png"/>
                            </div>
                            <div>
                                <img src="img/小米手机/tv3.png"/>
                            </div>
                            <div>
                                <img src="img/小米手机/tv4.png"/>
                            </div>
                            <div>
                                <img src="img/小米手机/tv5.png"/>
                            </div>
                        </div>
                    </div>
                </li>--%>
            </ul>


        </div>
        <div class="nav-search">
            <form action="/toSearch" method="post" id="myform">
                <input class="search-text" name="name" id="goodsName" type="text">
                <input class="iconfont search-btn " type="submit" value="&#xe7b7;">
            </form>
        </div>
    </div>
</div>

<div class="clear"></div>
