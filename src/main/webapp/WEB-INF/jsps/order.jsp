<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>订单预览页面</title>
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="icon" href="img/小米手机/logo-footer.png" type="image/gif">
    <link rel="stylesheet" href="css/layui/css/layui.css">
    <link href="css/jyc.css" rel="stylesheet"  type="text/css">
    <link rel="stylesheet" type="text/css" href="css/iconfont.css">
    <link rel="stylesheet" type="text/css" href="fonts/iconfont.css">

    <script src="css/layui/layui.js"></script>
    <script src="js/jquery-1.12.4.js"></script>
    <link rel="stylesheet" href="css/index.css">
    <script src="js/jyc.js"></script>

    <script type="text/javascript">
        $(function () {
            init();

            $("#btn_add").click(function () {
                var address = $("#address").val();
                if (address == "") {
                    alert("请填写有效地址");
                    return;
                }
                location.href = "OrderServlet?opr=addOrder&aid=" + $("#address").val();
            })


        })

        function init() {
            $.post("/getCartList", {null: null}, function (result) {
                var str = "";
                var i=1;
                var sum=0;
                $(result.data).each(function () {

                    str += "<tr>";
                    str += "<th>"
                    str+=i++;
                    str += "</th>";

                    str += "<th>" + this.goods.name + "</th>";
                    str += "<th>" + this.goods.price + "</th>";
                    str += "<th>" + this.num + "</th>";
                    str += "	<th>" + this.money + "</th>";
                    str += "</tr>";
                    sum+=this.money;

                });
                $("#sum").html("￥&nbsp;&nbsp;"+sum)
                $("#money").val(sum);

                $("#tbody").html(str);
            }, "JSON");



            $.post("/getAddressList", {null: null}, function (result) {
                var str = "";
                var i=1;
                var sum=0;
                var aid=0;
                if (result.data.length!=0){
                    $(result.data).each(function () {


                        if (this.level==1){
                            str += " <option value="+this.id+" selected>"+this.name+"&nbsp;&nbsp;"+this.phone+"&nbsp;&nbsp;"+this.detail+"</option>";
                        } else{
                            str += " <option value="+this.id+">"+this.name+"&nbsp;&nbsp;"+this.phone+"&nbsp;&nbsp;"+this.detail+"</option>";
                        }

                        aid=this.id;
                    });
                } else{
                    str += "<tr >";
                    str += "<td colspan='5' style='padding: 10px 10px'>";
                    str += "<h5>收货地址</h5>";
                    str += " <a href='self_info.jsp'>添加收货地址</a>";
                    str += " </td>";
                    str += "  </tr>";
                }
                $("#aid").val(aid);
                $("#address").html(str);
            }, "JSON");
        }
    </script>
</head>
<body style="background-color:#f5f5f5">
<%@ include file="header.jsp" %>
<div class="container" style="background-color: white;">
    <div class="row" style="margin-left: 40px">
        <h3>订单预览
            <small>温馨提示：请添加你要邮递到的地址</small>
        </h3>
    </div>
    <div class="row" style="margin-top: 40px;">
        <div class="col-md-10 col-md-offset-1">
            <table class="table table-bordered table-striped table-hover">
                <tr>
                    <th>序号</th>
                    <th>商品名称</th>
                    <th>价格</th>
                    <th>数量</th>
                    <th>小计</th>
                </tr>
                <tbody id="tbody"></tbody>
                <tr >
                <td colspan='5' style='padding: 10px 10px'>
                 <h5>收货地址</h5>
                 <select id='address' style='width:60%' class='form-control'>
                 </select>
                 </td>
                 </tr>";

            </table>
        </div>
    </div>
    <hr>
    <div class="row">
        <div style="margin-left: 40px;">
            <h4>商品金额总计：<span id="total" class="text-danger"><b id="sum" style="font-size: 20px"></b></span></h4>
        </div>
    </div>
    <div class="row pull-right" style="margin-right: 40px;">
        <div style="margin-bottom: 20px;">
            <form action="/addOrder" method="post">
                <input type="hidden" name="money" id="money"/>
                <input type="hidden" name="aid" id="aid"/>
            <button id="btn_add" class="btn  btn-danger btn-lg" type="submit">提交订单</button>
            </form>
        </div>
    </div>
</div>


<!-- 底部 -->
<%@ include file="footer.jsp" %>

</body>
</html>