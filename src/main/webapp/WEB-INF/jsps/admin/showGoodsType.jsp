<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../css/bootstrap.min.css" />
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
<script type="text/javascript">
	
	
	function init(){
		     var level=$("#level").val();
	         var name=$("#name").val();
	         $.post("/goToShowGoodsType",{level:level,name:name},function(result){
	             var str="";
	             var index=0;
	             $(result.data).each(function(){
	             index++;
	             		str+="<tr>";
								str+="<td>"+index+"</td>";
								str+="<td>"+this.name+"</td>";
								str+="<td>"+this.level+"</td>";		
								if(this.parentName==null){
									str+="<td>无所属类型</td>";
								}else{
								str+="<td>"+this.parentName+"</td>";
									
								}
								str+="<td>";
									str+="<button data-toggle='modal' data-target='#myModal"+this.id+"'>修改</button>&nbsp;&nbsp";
									str+="<button id='btn'  onclick='javascript:del(this,"+this.id+")'>删除</button>";
									str+="<input id='getId' type='hidden' value='"+this.id+"'/>";
								str+="</td>";
						 str+="</tr>";
	             
	             });
	             $("#tbody").html(str);
	         },"JSON");
	}
	
	$(function(){
		init();
		
	     $("#search").click(function(){
	         init();
	     });
	     
	     
	     $("#update_btn").click(function(){
	    	 flag=true;
	    	 var name=$("#uname").val();
	    	 var id=$("#id").val();
	    	 $.post("../AdminGoodsTypeServlet?opr=updateGoodsType",{name:name,id:id},function(result){
	    		 if(result){
	    			 alert("修改成功");
	    			 flag=true;
					}else{
						alert("修改失败");
						 flag=false;
					}
	    	 },"JSON");

	     });
	     
	     
	     
	     
	     $("#updinfo").click(function() {
			var id=$("#getId").val();
			var name=$("#gtyName").html();
			$("#id").val(id);
			$("#uname").html(name);
		});
		
	});
	
	
	
	function del(el,id) {
		if(!confirm("是否删除改类别？")){
			return;
		}
		$.post("/delGoodsTypeById",{id:id},function(result){
			if(result.code=="0"){
				alert("删除成功!");
				$(el).parent().parent().remove();
			}else{
				alert("删除失败!");
			}
		},"JSON");
	}
	

</script>

<title>商品分类</title>
</head>
<body>
	
	
<div class="row" style="width:98%;margin-left: 1%;">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				商品类型
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
						<div class="form-group form-inline">
							<span>商品等级</span>
							<input type="text" name="level" class="form-control" id="level">
						</div>
					</div>
					<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
						<div class="form-group form-inline">
							<span>商品名称</span>
							<input type="text" name="name" class="form-control" id="name">
						</div>
					</div>
					<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
						<button type="button" class="btn btn-primary" id="search"><span class="glyphicon glyphicon-search"></span></button>
					</div>
				</div>
				<div style="height: 400px;overflow: scroll;">
				<table id="tb_list" class="table table-striped table-hover table-bordered">
					<tr>
						<td>序号</td><td>类型</td><td>等级</td><td>所属类型</td><td>操作</td>
					</tr>
			<tbody id="tbody">
					<c:forEach items="${goodsTypeList}" var="gtype" varStatus="i">
					
					<div class="modal fade" tabindex="-1" role="dialog" id="myModal${gtype.id}">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content">
									<div class="modal-header">
										
										<h4 class="modal-title">修改类别</h4>
									</div>
									<form action="../AdminGoodsTypeServlet?opr=upd" method="post" class="form-horizontal">
										<div class="motal-body">
											<div class="form-group">
												<label class="col-sm-2 control-label">类型名称</label>
												<div class="col-sm-10">
													<input type="hidden" name="id" value="${gtype.id}" id="id">
													<input type="text" name="name" class="form-control" value="${gtype.name}" id="uname">
												</div>
											</div>
										</div>
										<div class="motal-footer text-center" style="padding-bottom: 20px">
											<input type="submit" class="btn btn-primary" id="update_btn" value="修改"/>
										</div>
									</form>
								</div>
							</div>
							</div>
					
					
					<tr>
						<td>${i.count}</td>
						<td id="gtyName">${gtype.name}</td>
						<td>${gtype.level}</td>
						<td>${gtype.parentName==null?'无所属类型':gtype.parentName}</td>
						<td>
							<button data-toggle="modal" data-target="#myModal${gtype.id}">修改</button>&nbsp;&nbsp;
							<button onclick="javascript:del(this,${gtype.id})">删除</button>
						</td>
					</tr>
					</c:forEach>
		    </tbody>
				</table>
				</div>
			</div>
			
		</div>
	</div>
</div>
</body>
</html>