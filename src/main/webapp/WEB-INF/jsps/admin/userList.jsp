<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>小米网后台主页-会员信息页面</title>
<link href="../css/bootstrap.min.css" rel="stylesheet">
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
	
<script type="text/javascript">
	// $(document).ready(function(){
	// 	loadUser();
	// })
	//连接servlet 获取 数据
	// function loadUser(){
	// 	$.ajax({
	// 		url:"../AdminUserServlet?opr=getUser",
	// 		method:"get",
	// 		success:function(data){
	// 			showMsg(data);
	// 		}
	// 	});
	// }

	//封禁用户
	function delUser(id,flag){
		var status=flag==0?"封禁":"解封";
		if(confirm("确认要"+status+"吗?")){
			$.post("/updUserFlag",{id:id,flag:flag},function(result){
				if(result.code=="0"){
					alert(status+"成功！");
			}else{
				alert(status+"失败!");
			}
				init();
			},"JSON");
		}

	}

	$(function() {
		init();

	})

	function search() {
		init();
	}

	function init() {
		var username = $("input[name='username']").val();
		var genders = $("input[name='gender']");
		var gender = "";
		for(var i=0;i<genders.length;i++){
			if(genders[i].checked){
				gender += genders[i].value;
			}
		}

		$.post("/getUserList",{username:username,gender:gender},function(data){
			var list = data.data;
			$("#tb_list").html("<tr class='tr_head'><td>编号</td><td>邮箱</td><td>姓名</td><td>性别</td><td>类别</td><td>操作</td></tr>");
			var i = 1;
			for(var u in list){
				//声明 tr  td  对象
				var tr = $("<tr></tr>");
				var td1 = $("<td>"+(i++)+"</td>");
				var td2 = $("<td>"+list[u].email+"</td>");
				var td3 = $("<td>"+list[u].username+"</td>");
				var td4 = $("<td>"+list[u].gender+"</td>");
				var td5 = $("<td>"+(list[u].role==0?"管理员":"会员")+"</td>");
				if(list[u].role!=0){
					if(list[u].flag!=0){
						var td6 = $("<td><a href='javascript:delUser("+list[u].id+",0)' class='btn btn-primary btn-xs' style='background-color:red'>封禁</a></td>");
					}else{
						var td6 = $("<td><a href='javascript:delUser("+list[u].id+",1)' class='btn btn-primary btn-xs' style='background-color:green'>解封</a></td>");
					}
				}else{
					var td6 = $("<td></td>");
				}

				//将td 添加到tr中
				tr.append(td1);
				tr.append(td2);
				tr.append(td3);
				tr.append(td4);
				tr.append(td5);
				tr.append(td6);
				$("#tb_list").append(tr);
			}
		},"JSON");

	}
</script>
</head>
<body>
	
	<div class="row" style="width: 100%;">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">会员列表</div>
					<div class="panel-body">
					<!-- 条件查询 -->
						<form action="" method="post">
						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<div class="form-group form-inline">
									<span>用户姓名</span>
									<input type="text" name="username" class="form-control">
								</div>
							</div>
							<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
								<div class="form-group form-inline">
									<span>性别</span>
									&nbsp;&nbsp;&nbsp;&nbsp;
									<label class="radio-inline">
									  <input type="radio" name="gender" value="男" > 男&nbsp;&nbsp;&nbsp;&nbsp;
									</label>
									<label class="radio-inline">
									  <input type="radio"name="gender" value="女"> 女
									</label>
								</div>
							</div>
							<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
								<button type="button" class="btn btn-primary"  onclick="search()"><span class="glyphicon glyphicon-search"></span></button>
							</div>
						</div>
						</form>
				<!-- 列表显示 -->
						<table id="tb_list" class="table table-striped table-hover table-bordered">
							
						</table>

					</div>
				</div>
			</div>
		</div>
</body>
</html>