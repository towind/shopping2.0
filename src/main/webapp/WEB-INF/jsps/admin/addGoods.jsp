<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../css/bootstrap.min.css" />
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/DatePicker.js"></script>
<script type="text/javascript">
	$(function () {
		$("#btnAddGoods").click(function () {
			var name=$("#name").val();
			var typeid=$("#typeid").val();
			var pubdate=$("#pubdate").val();
			var price=$("#price").val();
			var star=$("#star").val();
			var picture=$("#picture").val();
			var intro=$("#intro").val();

			$.post("/AddGoods",{name:name,typeid:typeid,pubdate:pubdate,price:price,star:star,picture:picture,intro:intro},function (result) {
				if(result.code=="0"){

				}
				location.href="/toAddGoods"
				alert(result.msg)

			},"JSON")
		})
	})
</script>
<title>商品添加页面</title>
</head>
<body>
	<div class="row" style="margin-left: 20px;">
		<form action="" method="post" enctype="multipart/form-data">
			<div>
				<h3>新增商品</h3>
			</div>
			<hr />
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group form-inline">
						<label>名称:</label>
						<input type="text" name="name" class="form-control" id="name"/>
					</div>
					
					<div class="form-group form-inline">
						<label>分类:</label>
						<select name="typeid" class="form-control" id="typeid">
							<option value="0">------</option>
							<c:forEach items="${goodsTypeList}" var="type" >
								<c:if test="${type.level ==1}">
									<option value="${type.id }">${type.name }</option>
								</c:if>
							</c:forEach>
						</select>
					</div>
					<div class="form-group form-inline">
						<label>时间:</label>
						<input type="text" name="pubdate" readonly="readonly" class="form-control" onclick="setday(this)" id="pubdate"/>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group form-inline">
						<label>价格:</label>
						<input type="text" name="price" class="form-control" id="price"/>
					</div>
					<div class="form-group form-inline">
						<label>评分:</label>
						<input type="text" name="star" class="form-control" id="star"/>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-10">
					<div class="form-group form-inline">
						<label>商品图片</label>
						<input type="file" name="picture" id="picture"/>
					</div>
					<div class="form-group ">
						<label>商品简介</label>
						<textarea  name="intro" class="form-control" rows="5" id="intro"></textarea>
					</div>
					<div class="form-group form-inline">
						<input type="button" value="添加" class="btn btn-primary" id="btnAddGoods"/>
						<input type="reset" value="重置" class="btn btn-default" />
					</div>
				</div>
			</div>
		</form>
	</div>
</body>
</html>