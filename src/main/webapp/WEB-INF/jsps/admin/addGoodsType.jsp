<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>.
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../css/bootstrap.min.css" />
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<title>添加商品种类</title>
<script type="text/javascript" src="js/jquery-1.12.4.js"></script>
<script type="text/javascript">
$(function(){
    $("#addLevelOne").click(function(){
         var goodsParent=$("[name=goodsParent]").val();
         var typename=$("[name=typename]").val();
         $.post("../AdminGoodsTypeServlet?opr=addLevel",{goodsParent:goodsParent,typename:typename},function(result){
                            if(result){
					             alert("新增成功！");
					             location.href="../admin/addGoodsType.jsp";
					         }else{
					              alert("新增失败！");
					         }
         },"JSON");
    });
     $("#addLevelTwo").click(function(){
         var typename=$("[name=name]").val();
         $.post("../AdminGoodsTypeServlet?opr=addLevel",{typename:typename},function(result){
                            if(result){
					             alert("新增成功！");
					             location.href="../admin/addGoodsType.jsp";
					         }else{
					              alert("新增失败！");
					         }
         },"JSON");
    });
    
    
})
</script>
</head>
<body>
<div style="width:98%;margin-left: 1%;">
	<div class="panel panel-default">
		<div class="panel-heading">
			添加商品种类
		</div>
		<div class="panel-body">
			<form action="../AdminGoodsTypeServlet" method="post" style="width: 430px;display:inline-block;">
				<div class="row">
					<div class="form-group form-inline">
						<span>添加一级种类</span>
					</div>
				</div>
				<div class="row">
					<div class="form-group form-inline">
						<span>种类名称</span>
						<input type="text" name="name" class="form-control">
					</div>
				</div>
				<div class="row">
					<div class="btn-group">
						<button type="reset" class="btn btn-default">清空</button>
						<button type="button" class="btn btn-default" id="addLevelTwo">添加</button>
					</div>
				</div>
			</form>
			<form action="../AdminGoodsTypeServlet" method="post" style="width: 430px;float:right;display:inline-block;">
				<div class="row">
					<div class="form-group form-inline">
						<span>所属种类</span>
						<select name="goodsParent">
							<option value="0">--请选择--</option>
							<c:forEach items="${goodsTypeList }" var="type">
								<c:if test="${type.level ==1}">
									<option value="${type.id }">${type.name }</option>
								</c:if>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="form-group form-inline">
						<span>种类名称</span>
						<input type="text" name="typename" class="form-control">
					</div>
				</div>
				<div class="row">
					<div class="btn-group">
						<button type="reset" class="btn btn-default">清空</button>
						<button type="button" class="btn btn-default" id="addLevelOne">添加</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
</body>
</html>