<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <link rel="stylesheet" media="screen" href="css1/login.css">
    <title>登录</title>
    <script type="text/javascript">
        $(function () {
            $("#eye").click(function () {
                if ($("#password").attr("type") == "password") {
                    $("#password").attr("type", "text");
                    $("#eye").attr("class", "glyphicon glyphicon-eye-close");
                } else {
                    $("#password").attr("type", "password");
                    $("#eye").attr("class", "glyphicon glyphicon-eye-open");
                }
            })
        })

		function toAdmin() {
			var username=$("#username").val();
			var password=$("#password").val();
			$.post("/toAdminLogin",{username:username,password:password},function (result) {
				if (result.code==0){
					location.href="/toShowAll"
				}else{
					$("#error").html(result.msg)
				}
			},"JSON")
		}
    </script>
    <style type="text/css">
        #main {
            position: absolute;
            width: 400px;
            height: 320px;
            left: 50%;
            top: 40%;
            margin-left: -200px;
            margin-top: -100px;
        }

        /*全局样式表*/
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
            background-image: url("img/backgroundmi.jpg");
            background-size: 120%;
            background-position: 50% 20%;
        }

        #app {
            position: absolute;
            width: 600px;
            height: 100px;
            left: 50%;
            top: 15%;
            margin-left: -307px;


        }

        #img {
            position: absolute;
            left: 10%;
            top: 5%;


        }

        #main {

            left: 50%;
            top: 46%;

        }
    </style>
</head>
<body>
<div id="img">
    <a href="index.html"><img src="img/小米手机/logo-footer.png"></a>
</div>
<div id="app">
    <p style="color: white;font-size: 28px;text-align: center">小米商城</p>
    <p style="color: white;font-size: 28px;text-align: center">享受科技带来的美好生活</p>
</div>
<div id="main" class="panel panel-primary">
	<div class="panel-heading">
		<div class="panel-title">
			后台登录
		</div>
	</div>
	<div class="panel-body">
		<div style="text-align: center;">
			<img src="../image/mistore_logo.png" alt="logo" width="30%" height="30%" />
		</div>
		<form action="javascript:void(0)" method="post">
			<div class="form-group">
				<label>用户名:</label>
				<input type="text" name="username" id="username" class="form-control" placeholder="请输入用户名"/>
			</div>
			<div class="form-group" id="one">
				<label>密&nbsp;&nbsp;&nbsp;码:</label>
				<div class="input-group">
					<input type="password" name="password" id="password" class="form-control"  placeholder="请输入密码"/>
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-eye-open" id="eye"></span>
					</span>
				</div>
			</div>
			<span style="color: red;font-size: 12px" id="error"></span>
			<div class="form-group" style="text-align: center;">
				<input type="button" value="登录" onclick="toAdmin()" class="btn btn-primary">
				<input type="reset" value="重置" class="btn btn-default">
			</div>
		</form>
	</div>
</div>




</body>
</html>