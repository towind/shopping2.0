<%--
  Created by IntelliJ IDEA.
  User: Gosick
  Date: 2022/4/10
  Time: 8:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title>Layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="css/layui/css/layui.css" media="all">
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
    <script type="text/javascript" src="css/layui/layui.js"></script>
    <script src="js/jquery-3.3.1.js"></script>
    <style type="text/css">
        .layui-side-scroll {
            height: 100%;
        }
    </style>
</head>
<body>
<div class="layui-layout layui-layout-admin site-demo-fixed">
    <div class="layui-header header header-demo">

        <ul class="layui-nav" lay-filter="">
            <a ><img src="img/小米手机/logo-footer.png"></a>
            <li class="layui-nav-item"><a href="">最新活动</a></li>
            <li class="layui-nav-item layui-this"><a href="">产品</a></li>
            <li class="layui-nav-item"><a href="#">大数据</a></li>
            <li class="layui-nav-item">
                <a href="javascript:">解决方案</a>
                <dl class="layui-nav-child"> <!-- 二级菜单 -->
                    <dd><a href="">移动模块</a></dd>
                    <dd><a href="">后台模版</a></dd>
                    <dd><a href="">电商平台</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item"><a href="">社区</a></li>
        </ul>

    </div>
    <div class="layui-side layui-bg-black">
        <ul class="layui-nav layui-nav-tree" lay-filter="test">
            <!-- 侧边导航: <ul class="layui-nav layui-nav-tree layui-nav-side"> -->
            <li class="layui-nav-item layui-nav-itemed">
                <a href="javascript:">用户管理</a>
                <dl class="layui-nav-child">
                    <dd><a href="javascript:void(0)" id="toUserList">会员管理</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item">
                <a href="javascript:">商品管理</a>
                <dl class="layui-nav-child">
                    <dd><a href="javascript:void(0)" id="toShowGoodsType">查看商品分类</a></dd>
                    <dd><a href="javascript:void(0)" id="toAddGoodsType">添加商品分类</a></dd>
                    <dd><a href="javascript:void(0)" id="toShowGoods">查看商品</a></dd>
                    <dd><a href="javascript:void(0)" id="toAddGoods">添加商品</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item">
                <a href="#">订单管理</a>
                <dl class="layui-nav-child">
                    <dd><a href="javascript:void(0)" id="toShowAllOrder">查看订单</a></dd>
                </dl>
            </li>
        </ul>
    </div>

    <div class="layui-body">
        <!-- 内容主体区域 -->
        <div style="padding: 15px;">
            <script type="text/javascript">
                $("#toUserList").click(function(){
                    $("#demoAdmin").attr("src","/toUserList");
                })
                $("#toShowGoodsType").click(function(){
                    $("#demoAdmin").attr("src","/toShowGoodsType");
                })
                $("#toAddGoodsType").click(function(){
                    $("#demoAdmin").attr("src","/toAddGoodsType");
                })
                $("#toShowGoods").click(function(){
                    $("#demoAdmin").attr("src","/toShowGoods");
                })
                $("#toAddGoods").click(function(){
                    $("#demoAdmin").attr("src","/toAddGoods");
                })
                $("#toShowAllOrder").click(function(){
                    $("#demoAdmin").attr("src","/goShowAllOrder");
                })
            </script>
            <iframe src=""  frameborder="0" id="demoAdmin" style="width: 100%; height: 100%; border-radius: 2px;"></iframe>
        </div>
    </div>


    <div class="layui-footer">
        <!-- 底部固定区域 -->
      <span style="color: #ff8237">用心保证品质，打造完美生活</span>
    </div>
</div>
<script>
    //注意：导航 依赖 element 模块，否则无法进行功能性操作
    layui.use('element', function () {
        var element = layui.element;

        //…
    });
</script>
</body>
</html>
