<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>后台 订单列表</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../css/bootstrap.min.css">
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/index.js"></script>

	<script type="text/javascript" src="js/jquery-1.12.4.js"></script>
	<script type="text/javascript">
	$(function () {
		init();

		$("#search").click(function () {
			$("#pageNo").val(1)
			init();
		})
		
	});
	function init() {
		var username= $("#username").val();
		var status= $("#status").val();
		var pageNo=$("#pageNo").val();
		var pageSize=$("#pageSize").val();
		$.post("/getShowAllOrder",{username:username,status:status,pageNo:pageNo,pageSize:pageSize},function (result) {

			var str="";
			var index=0;
			$(result.data.list).each(function () {
				index++
				str+="<tr>";
				str+="<td>"+index+"</td>";
				str+="<td>"+this.id+"</td>";
				str+="<td>"+this.money+"</td>";
				str+="<td>";
				if(this.status==1){
					str+="未支付";
				}else if(this.status==2){
					str+="已支付,待发货";
				}else if(this.status==3){
					str+="已发货,待收货";
				}else if(this.status==4){
					str+="订单完成";
				}
				str+="	</td>";
				str+="	<td>$"+this.time+"</td>";
				str+="	<td>"+this.user.username+"</td>";
				str+="	<td>";
				if(this.status==2){
					str+="<button type='button' class='btn btn-danger btn-sm' onclick='sendOrder(\""+this.id+"\")'>发货</button>";
				}
				str+="		</td>";
				str+="		</tr>";

			});
			$("#tbody").html(str);



			$("#nCurPage").html(result.data.pageNo);/*设置我们显示的是多少页*/
			$("#nPageSize").html(result.data.pageCount);/*设置我们显示最大的页吗*/
			$("#pageCount").val(result.data.pageCount)
		},"JSON");

	}
	function sendOrder(id) {
		$.post("/updStatus",{id:id},function (result) {
			if (result.code=="0"){
				init();
			}
			alert(result.msg)
	},"JSON");
	}


	function home() {
		$("#pageNo").val(1);
		init();
	}

	function per() {
		var pageNo= parseInt($("#pageNo").val());
		if(pageNo==1){
			return;
		}else{
			pageNo=pageNo-1;
		}
		$("#pageNo").val(pageNo);
		init();
	}

	function next() {
		var pageNo= parseInt($("#pageNo").val());
		//alert("1:currPage==>"+currPage);
		if(pageNo==parseInt($("#pageCount").val())){
			return;
		}else{
			pageNo=pageNo+1;
		}

		$("#pageNo").val(pageNo);
		//alert("2:currPage==>"+ $("#currPage").val());
		init();
	}

	function last() {
		$("#pageNo").val($("#pageCount").val());
		init();
	}


	
	</script>

</head>
<body>
<div class="row" style="width:98%;margin-left: 1%;margin-top: 5px;">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				订单列表
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
						<div class="form-group form-inline">
							<span>用户姓名</span>
							<input type="text" name="username" class="form-control"  id="username">
						</div>
					</div>
					<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
						<div class="form-group form-inline">
							<span>订单状态</span>
							<select name="status" class="form-control" id="status">
								<option value="">----------</option>
								<option value="1">未支付</option>
								<option value="2">已支付,待发货</option>
								<option value="3">已发货,待收货</option>
								<option value="4">完成订单</option>
							</select>
						</div>
					</div>
					<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
						<button type="button" class="btn btn-primary" id="search"><span class="glyphicon glyphicon-search"></span></button>
					</div>
				</div>
				
				<table id="tb_list" class="table table-striped table-hover table-bordered table-condensed">
					<tr>
						<td>序号</td>
						<td>订单编号</td>
						<td>总金额</td>
						<td>订单状态</td>
						<td>订单时间</td>
						<td>用户姓名</td>
						<td>操作</td>
					</tr>
					<tbody id="tbody">



					</tbody>
					<tfoot id="tfoot">
		    			<tr>
		    				<td colspan="8" align="center">
		    					 <a href="javascript:home()">首页</a>&nbsp;&nbsp;&nbsp;
		                		 <a href="javascript:per()">上一页</a>&nbsp;&nbsp;&nbsp;
		                		 <a href="javascript:next()">下一页</a>&nbsp;&nbsp;&nbsp;
		                		 <a href="javascript:last()">末页</a>&nbsp;&nbsp;&nbsp;
		                		 <!--nCurPage  当前页   nPageSize  最大页 -->
		    					 第<span id="nCurPage"></span>/<span id="nPageSize"></span>页
		    				</td>
		    			</tr>
		    		</tfoot>
				</table>
				<input type="hidden" id="pageNo" value="1">
				<input type="hidden" id="pageSize" value="10">
				<input type="hidden" id="pageCount" value="0">
		    
			</div>
		</div>
	</div>
</div>
</body>
</html>