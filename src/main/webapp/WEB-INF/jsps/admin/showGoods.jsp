<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../css/bootstrap.min.css" />
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/DatePicker.js"></script>
<script type="text/javascript">
	function del(el,id) {
		if(!confirm("是否要删除该商品及其购物车内的关联物!")){
			return;
		}
			$.post("/delGoods",{id:id},function(data){
				if(data){
					alert("删除成功!");
					$(el).parent().parent().remove();
				}else{
					alert("删除失败!");
				}
				init();
				
			},"JSON");
	}

	function updGoods(id){
		if(!confirm("是否要修改该商品及其购物车内的关联物!")){
			return;
		}
		var name=$("#name").val();
		var price=$("#price").val();
		var intro=$("#intro").val();
		$.post("/updGoodsById",{id:id,name:name,price:price,intro:intro},function (result) {
			if (result.code=="0"){

			}
			alert(result.msg)
			init();
		},"JSON")
	}
	
	$(function(){
		init();
	     $("#search").click(function(){
			 $("#currPageNo").val(1);
			 init();
	     })
	});

	function init() {
		var name=$("[name='name']").val();
		var pubdate=$("[name='pubdate']").val();
		var pageNo=$("#currPageNo").val();
		$.post("/toShowGoodsPage",{name:name,pubdate:pubdate,pageNo:pageNo},function(result){
			var str="";
			var index=0;
			$(result.data.list).each(function(){
				index++;
				str+="<tr>";
				str+="<td>"+index+"</td>";
				str+="<td>"+this.name+"</td>";
				str+="<td>"+this.price+"</td>";
				str+="<td>"+this.pubdate+"</td>";
				str+="<td>"+this.goodsType.name+"</td>";
				str+="<td><a href='javascript:void(0)' onclick='del(this,"+this.id+")'>删除</a> &nbsp;<button data-toggle='modal' data-target='#myModal"+this.id+"'>修改</button> &nbsp;";
				str+="<a tabindex='0' id='example"+this.id+"' class='btn btn-primary btn-xs'";
				str+=" role='button' data-toggle='popover'";
				str+=" data-trigger='focus'";
				str+=" data-placement='left'";
				str+="data-content='"+this.intro+"' onmouseover='javascript:$(\"#example"+this.id+"\").popover()'>描述</a>";

				str+="<div class='modal fade' tabindex='-1' role='dialog' id='myModal"+this.id+"'>";
				str+=" <div class='modal-dialog' role='document'>";
				str+="<div class='modal-content'>";
				str+="<div class='modal-header'>";
				str+="<button type='button'  data-dismiss='modal'>";
				str+="<span>&times;</span>";
				str+="</button>";
				str+="<h4 class='modal-title'>修改商品</h4>";
				str+="</div>";
				str+="<form action='javascript:void(0)' method='post' class='form-horizontal' >";
				str+="<div class='motal-body'>";
				str+="<div class='form-group'>";
				str+="<label class='col-sm-2 control-label'>商品名称</label>";
				str+="<div class='col-sm-10'>";
				str+="<input type='text' name='name'id='name' class='form-control' value='"+this.name+"'>";
				str+="</div>";
				str+="</div>";
				str+="<div class='form-group'>";
				str+="<label class='col-sm-2 control-label'>价格</label>";
				str+="<div class='col-sm-10'>";
				str+="<input type='text' name='price' id='price' class='form-control' value='"+this.price+"'>";
				str+="</div>";
				str+="</div>";
				str+="<div class='form-group'>";
				str+="<label class='col-sm-2 control-label'>描述</label>";
				str+="<div class='col-sm-10'>";
				str+="<input type='text' name='intro' id='intro' class='form-control' value='"+this.intro+"'>";
				str+="</div>";
				str+="</div>";
				str+="</div>";
				str+="<div class='motal-footer text-center' style='padding-bottom: 20px'>";
				str+="<button type=\"button\" class=\"btn btn-primary\" onclick=\"updGoods('"+this.id+"')\">修改</button>";
				str+="</div>";
				str+="</form>";
				str+="</div>";
				str+="</div>";
				str+="</div>";

				str+="</td>";
				str+="</tr>";

			});
			$("#tbody").html(str);
		},"JSON");
	}

	function home() {
		$("#currPageNo").val(1);
		init();
	}

	function per() {
		var currPage= parseInt($("#currPageNo").val());
		if(currPage==1){
			return;
		}else{
			currPage=currPage-1;
		}
		$("#currPageNo").val(currPage);
		init();
	}

	function next() {
		var currPage= parseInt($("#currPageNo").val());
		//alert("1:currPage==>"+currPage);
		if(currPage==parseInt($("#totalPageCount").val())){
			return;
		}else{
			currPage=currPage+1;
		}

		$("#currPageNo").val(currPage);
		//alert("2:currPage==>"+ $("#currPage").val());
		init();
	}

	function last() {
		$("#currPageNo").val( $("#totalPageCount").val());
		init();
	}


</script>

<title>商品列表</title>

</head>
<body>
<div class="row" style="width:98%;margin-left: 1%;">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				会员列表
			</div>
			<div class="panel-body">
				<form action="" method="post">
				<div class="row">
					<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
						<div class="form-group form-inline">
							<span>商品名称</span>
							<input type="text" name="name" class="form-control">
						</div>
					</div>
					<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
						<div class="form-group form-inline">
							<span>上架时间</span>
							<input type="text" readonly="readonly"  name="pubdate" class="form-control" onclick="setday(this)">
						</div>
					</div>
					<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
						<button type="button" class="btn btn-primary" id="search"><span class="glyphicon glyphicon-search"></span></button>
					</div>
				</div>
				</form>
				<div style="height: 400px;overflow: scroll;">
					<table id="tb_list" class="table table-striped table-hover table-bordered">
						<tr>
							<td>序号</td><td>商品名称</td><td>价格</td><td>上架时间</td><td>类型</td><td>操作</td>
						</tr>
						<tbody id="tbody">
						<c:forEach items="${info.list}" var="goods" varStatus="i">
							<tr>
								<td>${i.count}</td>
								<td>${goods.name}</td>
								<td>${goods.price}</td>
								<td>${goods.pubdate}</td>
								<td>${goods.goodsType.name}</td>
								<td><a href="javascript:void(0)" onclick="del(this,${goods.id})">删除</a> &nbsp;
								
								<div class="modal fade" tabindex="-1" role="dialog" id="myModal${goods.id}">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content">
									<div class="modal-header">
										<button type="button"  data-dismiss="modal">
											<span>&times;</span>
										</button>
										<h4 class="modal-title">修改商品</h4>
									</div>
									<form action="javascript:void(0)" method="post" class="form-horizontal" >
										<div class="motal-body">
											<div class="form-group">
												<label class="col-sm-2 control-label">商品名称</label>
												<div class="col-sm-10">
													<input type="text" name="name" id="name" class="form-control" value="${goods.name}">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">价格</label>
												<div class="col-sm-10">
													<input type="text" name="price" id="price" class="form-control" value="${goods.price}">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">描述</label>
												<div class="col-sm-10">
													<input type="text" name="intro" id="intro" class="form-control" value="${goods.intro}">
												</div>
											</div>
											
										</div>
										<div class="motal-footer text-center" style="padding-bottom: 20px">
											<button type="button" class="btn btn-primary" onclick="updGoods('${goods.id}')">修改</button>
										</div>
									</form>
								</div>
							</div>
							</div>
								
								<button data-toggle="modal" data-target="#myModal${goods.id}">修改</button> &nbsp;
									<a tabindex="0" id="example${goods.id}" class="btn btn-primary btn-xs"
									role="button" data-toggle="popover"
									data-trigger="focus"
									data-placement="left"
									data-content="${goods.intro}">描述</a>
									<script type="text/javascript">
										$(function(){
											$("#example${goods.id}").popover();
										})
									</script>
								</td>
							</tr>
						</c:forEach>
						</tbody>
						<tfoot id="tfoot">
						<tr>
							<td colspan="7" align="right">
								<a href="javascript:home()">首页</a>&nbsp;&nbsp;&nbsp; <a href="javascript:per()">上一页</a>&nbsp;&nbsp;&nbsp <a
									href="javascript:next()">下一页</a>&nbsp;&nbsp;&nbsp
								<a href="javascript:last()">末页</a>&nbsp;&nbsp;&nbsp <span>第<span id="nCurPage"></span>页/
                共<span id="nPageSize"></span>页</span>(共<span id="span_totalPage"></span>条记录)
							</td>
						</tr>
						</tfoot>
					</table>
				</div>

				<input type="hidden" id="currPageNo" value="1">
				<input type="hidden" id="pageSize" value="7">
				<input type="hidden" id="totalPageCount" value="0">
			</div>
			
		</div>
	</div>
</div>
</body>
</html>