<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>小米商城首页</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <link rel="icon" href="img/小米手机/logo-footer.png" type="image/gif" >
    <link rel="stylesheet" href="css/layui/css/layui.css">
    <link href="css/jyc.css" rel="stylesheet"  type="text/css">
    <link rel="stylesheet" type="text/css" href="css/iconfont.css">
    <link rel="stylesheet" type="text/css" href="fonts/iconfont.css">

    <script src="css/layui/layui.js"></script>

    <script src="js/jquery-3.3.1.js"></script>
    <link rel="stylesheet" href="css/index.css">
    <script src="js/jyc.js"></script>
    <script type="text/javascript">
        $(function () {
            layui.use('carousel', function(){
                var carousel = layui.carousel;
                //建造实例
                carousel.render({
                    elem: '#home-carousel'
                    ,width: '100%' //设置容器宽度
                    ,height: '460px'
                    ,arrow: 'always' //始终显示箭头
                    //,anim: 'updown' //切换动画方式

                });
            });
        })
    </script>

   <%-- <style type="text/css">
        #load{
            display: none;
        }
        #topbar-cart:hover+#load{
            display: inline-block;
        }
    </style>--%>

</head>

<body>
<%@ include file="header.jsp" %>
<!--主体-->
<div class="main">
    <!--导航栏-->
    <div class="home-hero-container wrap">
        <div class="home-carousel">
            <div class="layui-carousel" id="home-carousel" lay-filter="test1">
                <div carousel-item>
                    <div>
                        <img src="img/index/home-carousel1.jpg"/>
                    </div>
                    <div>
                        <img src="img/index/home-carouse2.jpg"/>
                    </div>
                    <div>
                        <img src="img/index/home-carouse3.jpg"/>
                    </div>
                    <div>
                        <img src="img/index/home-carouse4.jpg"/>
                    </div>
                    <div>
                        <img src="img/index/home-carouse5.jpg"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="home-category">
            <ul>
                <li class="nav-hover">
                    <a href="views/mobilephone.html">手机 电话卡<i class="iconfont">&#xe602;</i></a>
                    <div class="category-children">
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/note1-1.png"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/min21-2.png"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/xm61-3.png"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/80pc1-4.png"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/mix21-5.png"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/redmi51-6.png"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/note1-1.png"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/min21-2.png"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/xm61-3.png"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/80pc1-4.png"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/mix21-5.png"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/redmi51-6.png"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/note1-1.png"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/min21-2.png"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/xm61-3.png"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/80pc1-4.png"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/mix21-5.png"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/redmi51-6.png"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-hover">
                    <a href="views/TV.html">电视 盒子<i class="iconfont">&#xe602;</i></a>
                    <div class="category-children">
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/tv1-1.jpg"/>
                                    <span>小米电视4A 32英寸</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/tv1-2.jpg"/>
                                    <span>小米电视4A 40英寸</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/tv1-3.jpg"/>
                                    <span>小米电视4A 43英寸</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/tv1-4.jpg"/>
                                    <span>小米电视4A 49英寸</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/tv1-5.jpg"/>
                                    <span>小米电视4A 50英寸</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/tv1-6.jpg"/>
                                    <span>小米电视4A 55英寸</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/tv1-1.jpg"/>
                                    <span>小米电视4A 32英寸</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/tv1-2.jpg"/>
                                    <span>小米电视4A 40英寸</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/tv1-3.jpg"/>
                                    <span>小米电视4A 43英寸</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/tv1-4.jpg"/>
                                    <span>小米电视4A 49英寸</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/tv1-5.jpg"/>
                                    <span>小米电视4A 50英寸</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/tv1-6.jpg"/>
                                    <span>小米电视4A 55英寸</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/tv1-1.jpg"/>
                                    <span>小米电视4A 32英寸</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/tv1-2.jpg"/>
                                    <span>小米电视4A 40英寸</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/tv1-3.jpg"/>
                                    <span>小米电视4A 43英寸</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/tv1-4.jpg"/>
                                    <span>小米电视4A 49英寸</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/tv1-5.jpg"/>
                                    <span>小米电视4A 50英寸</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/tv1-6.jpg"/>
                                    <span>小米电视4A 55英寸</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-hover">
                    <a href="views/computer.html">笔记本<i class="iconfont">&#xe602;</i></a>
                    <div class="category-children">
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/computer1-1.jpg"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/computer1-2.jpg"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/computer1-3.jpg"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/computer1-4.jpg"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/computer1-5.jpg"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/computer1-6.jpg"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/computer1-1.jpg"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/computer1-2.jpg"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/computer1-3.jpg"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/computer1-4.jpg"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/computer1-5.jpg"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/computer1-6.jpg"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/computer1-1.jpg"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/computer1-2.jpg"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/computer1-3.jpg"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/computer1-4.jpg"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/computer1-5.jpg"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/computer1-6.jpg"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                    </div>

                </li>
                <li class="nav-hover">
                    <a href="views/Smart_Appliances.html">智能 家电<i class="iconfont">&#xe602;</i></a>
                    <div class="category-children">
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/note1-1.png"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/min21-2.png"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/xm61-3.png"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/80pc1-4.png"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/mix21-5.png"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/redmi51-6.png"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/note1-1.png"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/min21-2.png"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/xm61-3.png"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/80pc1-4.png"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/mix21-5.png"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/redmi51-6.png"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/note1-1.png"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/min21-2.png"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/xm61-3.png"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/80pc1-4.png"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/mix21-5.png"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/redmi51-6.png"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                    </div>

                </li>
                <li class="nav-hover">
                    <a href="views/Healthy_home.html">健康 家居 <i class="iconfont">&#xe602;</i></a>
                    <div class="category-children">
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/note1-1.png"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/min21-2.png"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/xm61-3.png"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/80pc1-4.png"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/mix21-5.png"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/redmi51-6.png"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/note1-1.png"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/min21-2.png"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/xm61-3.png"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/80pc1-4.png"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/mix21-5.png"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/redmi51-6.png"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/note1-1.png"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/min21-2.png"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/xm61-3.png"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/80pc1-4.png"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/mix21-5.png"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/redmi51-6.png"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                    </div>

                </li>
                <li class="nav-hover">
                    <a href="views/Travel_children.html ">出行 儿童<i class="iconfont">&#xe602;</i></a>
                    <div class="category-children">
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/note1-1.png"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/min21-2.png"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/xm61-3.png"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/80pc1-4.png"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/mix21-5.png"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/redmi51-6.png"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/note1-1.png"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/min21-2.png"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/xm61-3.png"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/80pc1-4.png"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/mix21-5.png"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/redmi51-6.png"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/note1-1.png"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/min21-2.png"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/xm61-3.png"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/80pc1-4.png"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/mix21-5.png"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/redmi51-6.png"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-hover">
                    <a href="views/Router.html">路由器 手机配件 <i class="iconfont">&#xe602;</i></a>
                    <div class="category-children">
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/note1-1.png"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/min21-2.png"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/xm61-3.png"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/80pc1-4.png"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/mix21-5.png"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/redmi51-6.png"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/note1-1.png"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/min21-2.png"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/xm61-3.png"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/80pc1-4.png"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/mix21-5.png"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/redmi51-6.png"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/note1-1.png"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/min21-2.png"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/xm61-3.png"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/80pc1-4.png"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/mix21-5.png"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/redmi51-6.png"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-hover">
                    <a href="views/power.html">移动电源 插线板<i class="iconfont">&#xe602;</i></a>
                    <div class="category-children">
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/note1-1.png"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/min21-2.png"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/xm61-3.png"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/80pc1-4.png"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/mix21-5.png"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/redmi51-6.png"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/note1-1.png"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/min21-2.png"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/xm61-3.png"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/80pc1-4.png"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/mix21-5.png"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/redmi51-6.png"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/note1-1.png"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/min21-2.png"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/xm61-3.png"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/80pc1-4.png"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/mix21-5.png"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/redmi51-6.png"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-hover">
                    <a href="views/EarPhone.html">耳机 音箱<i class="iconfont">&#xe602;</i></a>
                    <div class="category-children">
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/note1-1.png"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/min21-2.png"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/xm61-3.png"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/80pc1-4.png"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/mix21-5.png"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/redmi51-6.png"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/note1-1.png"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/min21-2.png"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/xm61-3.png"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/80pc1-4.png"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/mix21-5.png"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/redmi51-6.png"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/note1-1.png"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/min21-2.png"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/xm61-3.png"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/80pc1-4.png"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/mix21-5.png"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/redmi51-6.png"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-hover">
                    <a href="views/mi_life.html">生活 米兔<i class="iconfont">&#xe602;</i></a>
                    <div class="category-children">
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/note1-1.png"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/min21-2.png"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/xm61-3.png"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/80pc1-4.png"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/mix21-5.png"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/redmi51-6.png"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/note1-1.png"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/min21-2.png"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/xm61-3.png"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/80pc1-4.png"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/mix21-5.png"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/redmi51-6.png"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="children-list">
                            <li>
                                <a>
                                    <img src="img/index/nav-index/note1-1.png"/>
                                    <span>小米Note 3</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/min21-2.png"/>
                                    <span>小米MIX 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/xm61-3.png"/>
                                    <span>小米6</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/80pc1-4.png"/>
                                    <span>小米5X</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/mix21-5.png"/>
                                    <span>小米Max 2</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <img src="img/index/nav-index/redmi51-6.png"/>
                                    <span>红米5 Plus</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

            </ul>
        </div>
        <div class="home-hero-sub">
            <div class="span6">
                <ul>
                    <li>
                            <i class="iconfont"></i>
                            选购手机
                    </li>
                    <li>
                            <i class="iconfont"></i>
                            企业团购
                    </li>
                    <li>
                            <i class="iconfont"></i>
                            F码通道
                    </li>
                    <li>
                            <i class="iconfont"></i>
                            米粉卡
                    </li>
                    <li>
                            <i class="iconfont"></i>
                            以旧换新
                    </li>
                    <li>
                            <i class="iconfont"></i>
                            话费充值
                    </li>
                </ul>
            </div>
            <div class="row3">
                <div>
                    <img src="img/index/span6-1.jpg"/>
                </div>
                <div>
                    <img src="img/index/span6-2.jpg"/>
                </div>
                <div>
                    <img src="img/index/span6-3.jpg"/>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <!--开始闪购-->

    <!--结束闪购-->
    <div class="collect-card wrap">
        <img src="img/index/collect-card.jpg"/>
    </div>
</div>
<div class="main-page">
    <div class="wrap">
        <div class="phone">
            <div class="title">
                <h2>手机</h2>
                <div class="look-all">查看全部></div>
            </div>
            <div class="clear"></div>
            <div class="content">
                <div class="phone-left">
                    <img src="img/index/box-1.jpg"/>
                </div>
                <div class="phone-8">
                    <ul>
                        <li>
                            <div>
                                <img src="img/index/box8-1.jpg" height="220" width="220"/>
                                <h3>小米6 4GB+64GB</h3>
                                <p class="desc">变焦双摄，4 轴防抖，骁龙835 处理器</p>
                                <p class="price">2299 元</p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/box8-1.jpg" height="220" width="220"/>
                                <h3>小米6 4GB+64GB</h3>
                                <p class="desc">变焦双摄，4 轴防抖，骁龙835 处理器</p>
                                <p class="price">2299 元</p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/box8-1.jpg" height="220" width="220"/>
                                <h3>小米6 4GB+64GB</h3>
                                <p class="desc">变焦双摄，4 轴防抖，骁龙835 处理器</p>
                                <p class="price">2299 元</p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/box8-1.jpg" height="220" width="220"/>
                                <h3>小米6 4GB+64GB</h3>
                                <p class="desc">变焦双摄，4 轴防抖，骁龙835 处理器</p>
                                <p class="price">2299 元</p>
                            </div>
                        </li>
                        <li class="down">
                            <div>
                                <img src="img/index/box8-1.jpg" height="220" width="220"/>
                                <h3>小米6 4GB+64GB</h3>
                                <p class="desc">变焦双摄，4 轴防抖，骁龙835 处理器</p>
                                <p class="price">2299 元</p>
                            </div>
                        </li>
                        <li class="down">
                            <div>
                                <img src="img/index/box8-1.jpg" height="220" width="220"/>
                                <h3>小米6 4GB+64GB</h3>
                                <p class="desc">变焦双摄，4 轴防抖，骁龙835 处理器</p>
                                <p class="price">2299 元</p>
                            </div>
                        </li>
                        <li class="down">
                            <div>
                                <img src="img/index/box8-1.jpg" height="220" width="220"/>
                                <h3>小米6 4GB+64GB</h3>
                                <p class="desc">变焦双摄，4 轴防抖，骁龙835 处理器</p>
                                <p class="price">2299 元</p>
                            </div>
                        </li>
                        <li class="down">
                            <div>
                                <img src="img/index/box8-1.jpg" height="220" width="220"/>
                                <h3>小米6 4GB+64GB</h3>
                                <p class="desc">变焦双摄，4 轴防抖，骁龙835 处理器</p>
                                <p class="price">2299 元</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="collect-card">
            <img src="img/index/collect-card-2.jpg"/>
        </div>
        <div class="jiadian">
            <div class="title">
                <h2>家电</h2>
            </div>
            <div class="jiadian-more">
                <span><a class="tab-active">热门</a><a>出行</a><a>健康</a><a>酷玩</a><a>路由器</a></span>
            </div>
            <div class="clear"></div>
            <div class="content">
                <div class="box-8-left">
                    <img src="img/index/jiadian-left-1.jpg"/>
                    <img src="img/index/jiadian-left-2.jpg"/>
                </div>
                <div class="box-8">
                    <ul>
                        <li>
                            <div>
                                <img src="img/index/jiadian1.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/jiadian1.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/jiadian1.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/jiadian1.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li class="down">
                            <div>
                                <img src="img/index/jiadian1.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li class="down">
                            <div>
                                <img src="img/index/jiadian1.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li class="down">
                            <div>
                                <img src="img/index/jiadian1.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li class="down">
                            <div>
                                <img src="img/index/jiadian1.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
        <div class="collect-card">
            <img src="img/index/collect-card-3.jpg"/>
        </div>
        <div class="intelligence">
            <div class="title">
                <h2>智能</h2>
            </div>
            <div class="intelligence-more">
                <span><a class="tab-active">热门</a><a>电视影音</a><a>电脑</a><a>家居</a></span>
            </div>
            <div class="clear"></div>
            <div class="content">
                <div class="box-8-left">
                    <img src="img/index/intelligence-left-1.jpg"/>
                    <img src="img/index/intelligence-left-2.jpg"/>
                </div>
                <div class="box-8">
                    <ul>
                        <li>
                            <div>
                                <img src="img/index/jiadian1.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/jiadian1.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/jiadian1.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/jiadian1.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li class="down">
                            <div>
                                <img src="img/index/jiadian1.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li class="down">
                            <div>
                                <img src="img/index/jiadian1.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li class="down">
                            <div>
                                <img src="img/index/jiadian1.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li class="down">
                            <div>
                                <img src="img/index/jiadian1.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
        <div class="collect-card">
            <img src="img/index/collect-card-4.jpg"/>
        </div>
        <div class="collocation">
            <div class="title">
                <h2>搭配</h2>
            </div>
            <div class="collocation-more">
                <span><a class="tab-active">热门</a><a>耳机音响</a><a>电源</a><a>电池储存卡</a></span>
            </div>
            <div class="clear"></div>
            <div class="content">
                <div class="box-8-left">
                    <img src="img/index/intelligence-left-1.jpg"/>
                    <img src="img/index/intelligence-left-2.jpg"/>
                </div>
                <div class="box-8">
                    <ul>
                        <li>
                            <div>
                                <img src="img/index/collocation1.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/collocation2.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/collocation3.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/collocation4.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li class="down">
                            <div>
                                <img src="img/index/collocation5.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li class="down">
                            <div >
                                <img src="img/index/collocation6.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li class="down">
                            <div>
                                <img src="img/index/collocation7.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li class="down">
                            <div>
                                <img src="img/index/collocation7.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
        <div class="collect-card">
            <img src="img/index/collect-card-5.jpg"/>
        </div>
        <div class="parts">
            <div class="title">
                <h2>配件</h2>
            </div>
            <div class="parts-more">
                <span><a class="tab-active">热门</a><a>保护套</a><a>贴膜</a><a>其他配件</a></span>
            </div>
            <div class="clear"></div>
            <div class="content">
                <div class="box-8-left">
                    <img src="img/index/parts-left-1.jpg"/>
                    <img src="img/index/parts-left-2.jpg"/>
                </div>
                <div class="box-8">
                    <ul>
                        <li>
                            <div>
                                <img src="img/index/parts1.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/parts2.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/parts3.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li >
                            <div>
                                <img src="img/index/parts4.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li class="down">
                            <div>
                                <img src="img/index/parts5.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li class="down">
                            <div>
                                <img src="img/index/parts6.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li class="down">
                            <div>
                                <img src="img/index/parts7.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li class="down">
                            <div >
                                <img src="img/index/parts7.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
        <div class="collect-card">
            <img src="img/index/collect-card-6.jpg"/>
        </div>
        <div class="periphery">
            <div class="title">
                <h2>周边</h2>
            </div>
            <div class="periphery-more">
                <span><a class="tab-active">热门</a><a>居家</a><a>生活周边</a><a>箱包</a></span>
            </div>
            <div class="clear"></div>
            <div class="content">
                <div class="box-8-left">
                    <img src="img/index/periphery-left-1.jpg"/>
                    <img src="img/index/periphery-left-2.jpg"/>
                </div>
                <div class="box-8">
                    <ul>
                        <li>
                            <div>
                                <img src="img/index/periphery1.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/periphery2.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/periphery3.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/periphery4.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li class="down">
                            <div>
                                <img src="img/index/periphery5.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li class="down">
                            <div>
                                <img src="img/index/periphery6.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li class="down">
                            <div>
                                <img src="img/index/periphery7.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li class="down">
                            <div>
                                <img src="img/index/periphery7.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
        <div class="collect-card">
            <img src="img/index/collect-card-7.jpg"/>
        </div>

        <!--为你推荐-->
        <div class="recommended">
            <div class="title">
                <h2>为你推荐</h2>
                <div class="more">
                    <a><i class="iconfont">&#xe601;</i></a>
                    <a><i class="iconfont">&#xe602;</i></a>
                </div>
            </div>
            <div  class="clear"></div>
            <div class="content">
                <div class="box-5">
                    <ul>
                        <li>
                            <div>
                                <img src="img/index/periphery1.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/periphery2.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/periphery3.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/periphery4.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/periphery5.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="hot-product">
            <div class="title">
                <h2>热门产品</h2>
            </div>
            <div  class="clear"></div>
            <div class="content">
                <div class="box-4">
                    <ul>
                        <li>
                            <div>
                                <img src="img/index/hot-product1.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/hot-product2.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/hot-product3.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/hot-product4.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="mi-content">
            <div class="title">
                <h2>内容</h2>
            </div>
            <div  class="clear"></div>
            <div class="content">
                <div class="box-4">
                    <ul>
                        <li>
                            <div>
                                <img src="img/index/hot-product1.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/hot-product2.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/hot-product3.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/hot-product4.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="video">
            <div class="title">
                <h2>视频</h2>
            </div>
            <div  class="clear"></div>
            <div class="content">
                <div class="box-4">
                    <ul>
                        <li>
                            <div>
                                <img src="img/index/雷军.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/video2.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/雷军斯塔克.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="img/index/video4.jpg"/>
                                <h3>小米电视4A 32英寸</h3>
                                <p class="decs">64位四核处理器 / 1GB+4GB大内存</p>
                                <p class="price">999 元<del>1199元</del></p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div style="height: 40px;"></div>
</div>

<!--底部--><!-- 底部 -->
<%@ include file="footer.jsp" %>
</body>

</html>
