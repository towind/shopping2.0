function per() {

    var currPageNo = $("#currPageNo").val();
    if (currPageNo == 1) {
        return;
    } else {
        currPageNo = parseInt(currPageNo) - 1;
    }
    $("#currPageNo").val(currPageNo);
    initData();
}

function next() {

    var currPageNo = $("#currPageNo").val();
    var totalPageCount = $("#totalPageCount").val();
    if (currPageNo == totalPageCount) {
        return;
    } else {
        currPageNo = parseInt(currPageNo) + 1;
    }
    $("#currPageNo").val(currPageNo);
    initData();
}

function initData() {
    var pageNo = $("#currPageNo").val();
    var typeId = $("#typeId").val();
    var name = $("#name").val();

    $.post("/toPage", {pageNo: pageNo, typeId: typeId, name: name}, function (data) {
        var str = "";
        $(data.data.list).each(function () {
            str+="<a href='/toShow?id="+this.id+"' target='_blank'>"
            str += "<div class='col-sm-3'>";
            str += "<div class='thumbnail'>";
            str += "<img src='img/picture/" + this.picture + "' style='width: 180px;height: 180px;' />";
            str += "<div class='caption'>";
            str += "<p>" + this.name + "</p>";
            str += "<p>热销指数";
            for (var num = 0; num < this.star; num++) {
                str += "<img src='image/star_red.gif' alt='star'/>";
            }
            str += "</p>";
            str += "<p>上架日期:" + this.pubdate + "</p>";
            str += "<p style='color:orange'>价格:" + this.price + "</p>";
            str += "</div>";
            str += "</div>";
            str += "</div>";
            str += "</a>";
        });


        $("#tbody").html(str);
        $("#currPageNo").val(data.data.pageNo);
        $("#totalPageCount").val(data.data.pageCount == 0 ? 1 : data.data.pageCount);
        var title=$("[name=title]")
        $("[name=title]").css("background-color","")
       title.each(function () {
           console.log($(this).html())
            if (data.data.pageNo==$(this).html()) {
                $(this).css("background-color","#bdbdbd")
            }
        })


    }, "JSON");

}


function goto(num) {
    $("#currPageNo").val(num);
    initData()
}