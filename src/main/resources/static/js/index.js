function home() {
	$("#currPageNo").val(1);
	initData();
}
function last() {
	//获取到最大值的的文本
	var pageCount=$("#nPageSize").html();
	$("#currPageNo").val(pageCount);
	initData();
}
function per() {
	//获取到当前页面的值
	var currPageNo=$("#currPageNo").val();
	if(currPageNo==1){
		return;
	}else{
		currPageNo=parseInt(currPageNo)-1;
	}
	$("#currPageNo").val(currPageNo);
	initData();
}
function next() {
	//获取到当前页面的值
	var currPageNo=$("#currPageNo").val();
	//获取到最大值的的文本
	var pageCount=$("#nPageSize").html();
	if(currPageNo==pageCount){
		return;
	}else{
		currPageNo=parseInt(currPageNo)+1;
	}
	$("#currPageNo").val(currPageNo);
	initData();
}

function initData() {
	var currPageNo=$("#currPageNo").val();
	
	var username=$("#username").val();
	var status=$("#status").val();
	
	$.get("../AdminOrderServlet?opr=changePage",{currPageNo:currPageNo,status:status,username:username},function(data){
		var str="";
		var index=0;
		$(data.list).each(function(){
			index++;
				str+="<tr>";
					str+="<td>"+index+"</td>";
					str+="<td>"+this.id+"</td>";
					str+="<td>"+this.money+"</td>";
					str+="<td>";
						if(this.status==1){
							str+="未支付";
						
						}else if(this.status==2){
							str+="已支付,待发货";
						
						}else if(this.status==3){
							str+="已发货,待收货";
						
						}else if(this.status==4){
							str+="订单完成";
						}
					str+="</td>";
					str+="<td>"+this.time+"</td>";
					str+="<td>"+this.user.username+"</td>";
					str+="<td>";
					if(this.status==2){
						str+="<button type='button' class='btn btn-danger btn-sm' onclick='sendOrder(\""+this.id+"\")'>发货</button>";
						}
					str+="</td>";
				str+="</tr>";
			})
		
			$("#tbody").html(str);
			$("#currPageNo").val(data.pageNo);
			$("#totalPageCount").val(data.pageCount==0?1:data.pageCount);
			$("#nCurPage").html(data.pageNo);
			$("#nPageSize").html(data.pageCount==0?1:data.pageCount);
	
	},"JSON");
	
}

function sendOrder(id){
	var currPageNo=$("#currPageNo").val();
	var username = $("input[name='username']").val();
			var status = $("select[name='orderStatus'] option:selected").val();
			$.post("../AdminOrderServlet?opr=upd",{id:id,username:username,status:status,currPageNo:currPageNo},function(data){
				var str="";
				var index=0;
				$(data.list).each(function() {
				index++;
				
					str+="<tr>";
						str+="<td>"+index+"</td>";
						str+="<td>"+this.id+"</td>";
						str+="<td>"+this.money+"</td>";
						str+="<td>";
							if(this.status==1){
								str+="未支付";
							
							}else if(this.status==2){
								str+="已支付,待发货";
							
							}else if(this.status==3){
								str+="已发货,待收货";
							
							}else if(this.status==4){
								str+="订单完成";
							}
						str+="</td>";
						str+="<td>"+this.time+"</td>";
						str+="<td>"+this.user.username+"</td>";
						str+="<td>";
						if(this.status==2){
							str+="<button type='button' class='btn btn-danger btn-sm' onclick='sendOrder(\""+this.id+"\")'>发货</button>";
							}
						str+="</td>";
					str+="</tr>";
				})
				$("#tbody").html(str);
				$("#currPageNo").val(data.pageNo);
				$("#totalPageCount").val(data.pageCount==0?1:data.pageCount);
				$("#nCurPage").html(data.pageNo);
				$("#nPageSize").html(data.pageCount==0?1:data.pageCount);
			},"JSON");
	}
	$(function(){
		$("#search").click(function(){
			$("#currPageNo").val(1);
			var username = $("input[name='username']").val();
			var status = $("select[name='orderStatus'] option:selected").val();
			
			
			$.post("../AdminOrderServlet?opr=select",{username:username,status:status},function(data){
				var str="";
				var index=0;
				$(data.list).each(function() {
				index++;
				
					str+="<tr>";
						str+="<td>"+index+"</td>";
						str+="<td>"+this.id+"</td>";
						str+="<td>"+this.money+"</td>";
						str+="<td>";
							if(this.status==1){
								str+="未支付";
							
							}else if(this.status==2){
								str+="已支付,待发货";
							
							}else if(this.status==3){
								str+="已发货,待收货";
							
							}else if(this.status==4){
								str+="订单完成";
							}
						str+="</td>";
						str+="<td>"+this.time+"</td>";
						str+="<td>"+this.user.username+"</td>";
						str+="<td>";
						if(this.status==2){
							str+="<button type='button' class='btn btn-danger btn-sm' onclick='sendOrder(\""+this.id+"\")'>发货</button>";
							}
						str+="</td>";
					str+="</tr>";
				})
				$("#tbody").html(str);
				$("#currPageNo").val(data.pageNo);
				$("#totalPageCount").val(data.pageCount==0?1:data.pageCount);
				$("#nCurPage").html(data.pageNo);
				$("#nPageSize").html(data.pageCount==0?1:data.pageCount);
			},"JSON");
		})
	})

